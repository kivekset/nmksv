const request = require("supertest");
const { mockSendMail } = require("../../__mocks__/nodemailer");

const {
  clearDb,
  getToken,
  stringGen,
  updateApply,
  saveRequestMockData,
} = require("../_helpers");
const databases = require("../../src/databases.js");

const app = require("../../src/app");

let readToken, deleteToken, updateToken, wrongToken;

beforeAll(async () => {
  readToken = await getToken("read:applicationrequests");
  deleteToken = await getToken("delete:applicationrequests");
  updateToken = await getToken("update:applicationrequests");
  wrongToken = await getToken("read:applications");

  await clearDb("metadata_db");
});

beforeEach(() => {
  return clearDb("application_request_db");
});

describe("POST /applications/request", () => {
  beforeEach(() => {
    return mockSendMail.mockClear();
  });

  it("Post valid request", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: "ville.vallaton@tuni.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining({
            ...doc,
            emailSent: true,
            applicationId: "",
            applicationSent: false,
          }),
        }),
      ])
    );
    expect(mockSendMail).toHaveBeenCalledTimes(1);
  });

  it("Post request with maximum length fields", async () => {
    // Arrange
    let doc = {
      name: stringGen(40),
      email: stringGen(92) + "@tuni.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining({
            ...doc,
            emailSent: true,
            applicationId: "",
            applicationSent: false,
          }),
        }),
      ])
    );
    expect(mockSendMail).toHaveBeenCalledTimes(1);
  });

  it("Posting request with too long name", async () => {
    // Arrange
    let doc = {
      name: stringGen(41),
      email: "random@tuni.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request with too long email", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: stringGen(93) + "@tuni.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request without name", async () => {
    // Arrange
    let doc = {
      email: "random@tuni.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request without email", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request without data approval", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: "random@tuni.fi",
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request with false data approval", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: "random@tuni.fi",
      dataApproval: false,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request with other than @tuni.fi email", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: "random@random.fi",
      dataApproval: true,
    };

    await updateApply(true);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request with email already existing", async () => {
    // Arrange
    await updateApply(true);
    let dbData = await saveRequestMockData(1);

    let doc = {
      name: "Ville Vallaton",
      email: dbData[0].doc.email,
      dataApproval: true,
    };

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[0].doc),
        }),
      ])
    );
    expect(mockSendMail).not.toHaveBeenCalled();
  });

  it("Posting request with apply closed", async () => {
    // Arrange
    let doc = {
      name: "Ville Vallaton",
      email: "random@tuni.fi",
      dataApproval: true,
    };

    await updateApply(false);

    // Act
    const res = await request(app).post(`/applications/request`).send(doc);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(403);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMail).not.toHaveBeenCalled();
  });
});

describe("POST /applications/applicationrequest/:id/bypasswait", () => {
  it("Bypass wait time succesfully", async () => {
    // Arrange
    let dbData = await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .post(`/applications/applicationrequest/${dbData[0].id}/bypasswait`)
      .set("Authorization", updateToken);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining({
            ...dbData[0].doc,
            bypassWaitTime: true,
          }),
        }),
      ])
    );
  });

  it("Incorrect ID returns 404", async () => {
    // Arrange
    await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .post(`/applications/applicationrequest/wrong-id/bypasswait`)
      .set("Authorization", updateToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).post(
      "/applications/applicationrequest/random-id/bypasswait"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .post("/applications/applicationrequest/random-id/bypasswait")
      .set("Authorization", wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /applications/request/:id", () => {
  it("Valid request found", async () => {
    // Arrange
    await updateApply(true);
    let dbData = await saveRequestMockData(1);

    // Act
    const res = await request(app).get(`/applications/request/${dbData[0].id}`);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(expect.objectContaining(dbData[0].doc));
  });

  it("Invalid ID returns 404", async () => {
    // Arrange
    await updateApply(true);
    await saveRequestMockData(1);

    // Act
    const res = await request(app).get(`/applications/request/wrong-id`);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Already sent application returns 403", async () => {
    // Assert
    await updateApply(true);
    let dbData = await saveRequestMockData(1, [{ applicationSent: true }]);

    // Act
    const res = await request(app).get(`/applications/request/${dbData[0].id}`);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /applications/applicationrequest/:id", () => {
  it("Correct ID returns correct request", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);

    // Act
    const res = await request(app)
      .get(`/applications/applicationrequest/${dbData[1].id}`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(expect.objectContaining(dbData[1].doc));
  });

  it("Incorrect ID returns 404", async () => {
    // Arrange
    await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .get(`/applications/applicationrequest/wrong-id`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).get(
      "/applications/applicationrequest/random-id"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get("/applications/applicationrequest/random-id")
      .set("Authorization", wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("DELETE /applications/applicationrequest/:id", () => {
  it("Correct document deleted", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);

    // Act
    const res = await request(app)
      .delete(`/applications/applicationrequest/${dbData[1].id}`)
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Nothing deleted with wrong ID", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);

    // Act
    const res = await request(app)
      .delete(`/applications/applicationrequest/wrong-id`)
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[0].doc),
        }),
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).delete(
      "/applications/applicationrequest/random-id"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .delete("/applications/applicationrequest/random-id")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /applications/applicationrequests", () => {
  it("Zero requests", async () => {
    // Act
    const res = await request(app)
      .get("/applications/applicationrequests")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).not.toBeNull();
    expect(res.body).toHaveLength(0);
  });

  it("One request", async () => {
    // Arrange
    let dbData = await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .get("/applications/applicationrequests")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0]).toEqual(expect.objectContaining(dbData[0].doc));
  });

  it("Many requests", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);

    // Act
    const res = await request(app)
      .get("/applications/applicationrequests")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc),
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).get("/applications/applicationrequests");

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get("/applications/applicationrequests")
      .set("Authorization", wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /applications/applicationrequest/delete", () => {
  it("Delete many existing feedbacks", async () => {
    // Arrange
    let dbData = await saveRequestMockData(3);

    // Act
    const res = await request(app)
      .post(`/applications/applicationrequest/delete`)
      .send({ ids: [dbData[1].id, dbData[2].id] })
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[2].doc),
        }),
      ])
    );
  });

  it("Delete many feedbacks but some not found", async () => {
    // Arrange
    let dbData = await saveRequestMockData(3);

    // Act
    const res = await request(app)
      .post(`/applications/applicationrequest/delete`)
      .send({ ids: [dbData[1].id, "wrong-id"] })
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Empty ID list returns 400", async () => {
    // Act
    const res = await request(app)
      .post("/applications/applicationrequest/delete")
      .send({ ids: [] })
      .set("Authorization", deleteToken);

    // Assert
    expect(res.statusCode).toEqual(400);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).post(
      "/applications/applicationrequest/delete"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .post("/applications/applicationrequest/delete")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

const request = require("supertest")
const fs = require("fs");

const { 
  clearDb, 
  getToken,
  saveRequestMockData,
  saveFeedbackMockData,
  saveApplicationMockData } = require("../_helpers");
const databases = require("../../src/databases.js");

const app = require('../../src/app');

let adminToken, wrongToken;

beforeAll(async () => {
  adminToken = await getToken("all:database");
  wrongToken = await getToken("read:applications");
});

describe("GET /deleted/application/attachment/:id/:name", () => {
  beforeEach(async () => {
    return clearDb("application_db");
  });

  it("Valid ID and name succeeds", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/attachment/${dbData[0].id}/esisku_logo.png`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.headers["content-type"]).toMatch(/png/);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Invalid ID fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/attachment/invalid-id/esisku_logo.png`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("invalid name fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/attachment/${dbData[0].id}/invalid-name`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Attachment of non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    // Act
    const res = await request(app)
      .get(`/deleted/application/attachment/${dbData[0].id}/esisku_logo.png`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Attachment of updated, non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);
    let putData = await databases.getApplicationDB().put({ _id: dbData[0].id, _rev: dbData[0].rev, update: "updated" });

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", putData.rev, file, "image/png");

    // Act
    const res = await request(app)
      .get(`/deleted/application/attachment/${dbData[0].id}/esisku_logo.png`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/application/attachment/random-id/random-name');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/application/attachment/random-id/random-name')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/request/:id", () => {
  beforeEach(async () => {
    return clearDb("application_request_db");
  });

  it("Correct document returned with valid ID", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
      expect.objectContaining(dbData[0].doc)
    );
  });

  it("ID of existing non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("ID of existing, once updated non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Invalid ID fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/request/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/request/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/feedback/:id", () => {
  beforeEach(async () => {
    return clearDb("feedback_db");
  });

  it("Correct document returned with valid ID", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
      expect.objectContaining(dbData[0].doc)
    );
  });

  it("ID of existing non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("ID of existing, once updated non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Invalid ID fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/feedback/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/feedback/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/application/:id", () => {
  beforeEach(async () => {
    return clearDb("application_db");
  });

  it("Correct document returned with valid ID", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
      expect.objectContaining(dbData[0].doc)
    );
  });

  it("Attachment is retrieved with the document", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
      expect.objectContaining({ 
        ...dbData[0].doc,
        _attachments: {
          "esisku_logo.png": expect.any(Object)
        }
      })
    );
  });

  it("ID of existing non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("ID of existing, once updated non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Invalid ID fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/application/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/application/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/request", () => {
  beforeEach(async () => {
    return clearDb("application_request_db");
  });

  it("Zero deleted documents", async () => {
    // Arrange
    await saveRequestMockData(2);

    // Act
    const res = await request(app)
      .get(`/deleted/request`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(0);
  });

  it("One deleted document", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Many deleted documents", async () => {
    // Arrange
    let dbData = await saveRequestMockData(3);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);
    await databases.getRequestDB().remove(dbData[1].id, dbData[1].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc)
      ])
    );
  });

  it("Only deleted one returned when non-deleted is updated", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/request`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/request');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/request')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/application", () => {
  beforeEach(async () => {
    return clearDb("application_db");
  });

  it("Zero deleted documents", async () => {
    // Arrange
    await saveApplicationMockData(2);

    // Act
    const res = await request(app)
      .get(`/deleted/application`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(0);
  });

  it("One deleted document", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Many deleted documents", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(3);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);
    await databases.getApplicationDB().remove(dbData[1].id, dbData[1].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc)
      ])
    );
  });

  it("Attachment retrieved with document", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          ...dbData[0].doc,
          _attachments: {
            "esisku_logo.png": expect.any(Object)
          }
        })
      ])
    );
  });

  it("Only deleted one returned when non-deleted is updated", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/application`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/application');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/application')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /deleted/feedback", () => {
  beforeEach(async () => {
    return clearDb("feedback_db");
  });

  it("Zero deleted documents", async () => {
    // Arrange
    await saveFeedbackMockData(2);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(0);
  });

  it("One deleted document", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Many deleted documents", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(3);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);
    await databases.getFeedbackDB().remove(dbData[1].id, dbData[1].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc)
      ])
    );
  });

  it("Only deleted one returned when non-deleted is updated", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, update: "updated" });
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .get(`/deleted/feedback`)
      .set('Authorization', adminToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc)
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/feedback');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/feedback')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /deleted/feedback/:id", () => {
  beforeEach(async () => {
    return clearDb("feedback_db");
  });

  it("Restoring with valid ID succeeds", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/feedback/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Latest version is restored with updated document", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    let putData = await databases.getFeedbackDB().put({ ...dbData[0].doc, _id: dbData[0].id, _rev: dbData[0].rev, name: "updated" });
    await databases.getFeedbackDB().remove(dbData[0].id, putData.rev);

    // Act
    const res = await request(app)
      .post(`/deleted/feedback/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining({ ...dbData[0].doc, name: "updated" }) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Restoring with invalid ID fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/feedback/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/feedback/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of updated, non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);
    await databases.getFeedbackDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, updated: "updated" });
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/feedback/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/feedback/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/feedback/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /deleted/request/:id", () => {
  beforeEach(async () => {
    return clearDb("application_request_db");
  });

  it("Restoring with valid ID succeeds", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/request/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getRequestDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Latest version is restored with updated document", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    let putData = await databases.getRequestDB().put({ ...dbData[0].doc, _id: dbData[0].id, _rev: dbData[0].rev, name: "updated" });
    await databases.getRequestDB().remove(dbData[0].id, putData.rev);

    // Act
    const res = await request(app)
      .post(`/deleted/request/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getRequestDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining({ ...dbData[0].doc, name: "updated" }) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Restoring with invalid ID fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/request/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getRequestDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/request/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getRequestDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of updated, non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveRequestMockData(2);
    await databases.getRequestDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, updated: "updated" });
    await databases.getRequestDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/request/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getRequestDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/request/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/request/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /deleted/application/:id", () => {
  beforeEach(async () => {
    return clearDb("application_db");
  });

  it("Restoring with valid ID succeeds", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Latest version is restored with updated document", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    let putData = await databases.getApplicationDB().put({ ...dbData[0].doc, _id: dbData[0].id, _rev: dbData[0].rev, name: "updated" });
    await databases.getApplicationDB().remove(dbData[0].id, putData.rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining({ ...dbData[0].doc, name: "updated" }) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Attachments are restored with the document", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    let file = fs.readFileSync(__dirname + "\\..\\data\\attachments\\esisku_logo.png");
    let putDoc = await databases.getApplicationDB()
      .putAttachment(dbData[0].id, "esisku_logo.png", dbData[0].rev, file, "image/png");

    await databases.getApplicationDB().remove(dbData[0].id, putDoc.rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/${dbData[0].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ 
          doc: expect.objectContaining({
            ...dbData[0].doc,
            _attachments: {
              "esisku_logo.png": expect.any(Object)
            }
          })
        }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Restoring with invalid ID fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/invalid-id`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Restoring of updated, non-deleted document fails", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);
    await databases.getApplicationDB().put({ _id: dbData[1].id, _rev: dbData[1].rev, updated: "updated" });
    await databases.getApplicationDB().remove(dbData[0].id, dbData[0].rev);

    // Act
    const res = await request(app)
      .post(`/deleted/application/${dbData[1].id}`)
      .set('Authorization', adminToken);

    // Assert
    const newData = await databases.getApplicationDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/deleted/application/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/deleted/application/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});
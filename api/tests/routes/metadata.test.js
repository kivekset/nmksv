const request = require("supertest")
const { mockSendMessage } = require("../../__mocks__/telegraf");

const { 
  clearDb, 
  getToken,
  updateApply,
  saveFeedbackMockData, 
  saveRequestMockData, 
  saveApplicationMockData 
} = require("../_helpers");
const databases = require("../../src/databases.js");

const app = require('../../src/app')

let emptyToken, updateOpenidToken, databaseToken;

beforeAll(async () => {
  emptyToken = await getToken();
  updateOpenidToken = await getToken("openid update:applyopen");
  databaseToken = await getToken("all:database");

  await clearDb("metadata_db");
});

describe("GET /metadata/applyopenpublic", () => {
  it("Correct state returned", async () => {
    // Arrange
    await updateApply(false, 2);

    // Act
    const res = await request(app)
      .get(`/metadata/applyopenpublic`)

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(false);
  });
});

describe("GET /metadata/applyopen", () => {
  it("Correct data returned", async () => {
    // Arrange
    let dbData = await updateApply(true, 2);

    // Act
    const res = await request(app)
      .get(`/metadata/applyopen`)
      .set("Authorization", emptyToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(expect.objectContaining(dbData));
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/metadata/applyopen');

    // Assert
    expect(res.statusCode).toEqual(401);
  });
});

describe("PUT /metadata/applyopen", () => {
  beforeEach(async () => {
    mockSendMessage.mockClear();

    await clearDb("metadata_db");
  });

  it("Update to new state succeeds, empty history", async () => {
    // Arrange
    await updateApply(true);

    // Act
    const res = await request(app)
      .put('/metadata/applyopen')
      .send({ open: false })
      .set('Authorization', updateOpenidToken);

    // Assert
    const data = await databases.getMetadataDB().get("applyMetaData");

    expect(res.statusCode).toEqual(200);
    expect(data.isManuallyOpen).toEqual(false);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
    expect(data.updates).toHaveLength(1);
    expect(data.updates).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          email: process.env.TEST_AUTH_USER,
          previousState: true,
          newState: false
        })
      ])
    );
  });

  it("Update to same state succeeds, semi-full history", async () => {
    // Arrange
    await updateApply(false, 4);

    // Act
    const res = await request(app)
      .put('/metadata/applyopen')
      .send({ open: false })
      .set('Authorization', updateOpenidToken);

    // Assert
    const data = await databases.getMetadataDB().get("applyMetaData");

    expect(res.statusCode).toEqual(200);
    expect(data.isManuallyOpen).toEqual(false);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
    expect(data.updates).toHaveLength(5);
    expect(data.updates).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          email: process.env.TEST_AUTH_USER,
          previousState: false,
          newState: false
        })
      ])
    );
  });

  it("Update is added to full history and oldest is removed", async () => {
    // Arrange
    const dbData = await updateApply(false, 10);
    dbData.updates.sort((a, b) => new Date(b.datetime) - new Date(a.datetime));
    const oldest = dbData.updates[dbData.updates.length - 1];

    // Act
    const res = await request(app)
      .put('/metadata/applyopen')
      .send({ open: true })
      .set('Authorization', updateOpenidToken);

    // Assert
    const data = await databases.getMetadataDB().get("applyMetaData");

    expect(res.statusCode).toEqual(200);
    expect(data.isManuallyOpen).toEqual(true);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
    expect(data.updates).toHaveLength(10);
    expect(data.updates).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          email: process.env.TEST_AUTH_USER,
          previousState: false,
          newState: true
        })
      ])
    );

    expect(data.updates).toEqual(expect.not.arrayContaining([oldest]));
  });

  it("Invalid payload fails", async () => {
    // Act
    const res = await request(app)
      .put('/metadata/applyopen')
      .send({ open: "invalid value" })
      .set('Authorization', updateOpenidToken);

    // Assert
    expect(res.statusCode).toEqual(400);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
      .put('/metadata/applyopen');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .put('/metadata/applyopen')
      .set('Authorization', databaseToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /metadata/adminstats", () => {
  beforeEach(async () => {
    await clearDb("application_db");
    await clearDb("application_request_db");
    await clearDb("feedback_db");
  });

  it("Correct data returned", async () => {
    // Arrange
    await updateApply(true, 2);
    await saveFeedbackMockData(3);
    await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .get(`/metadata/adminstats`)
      .set("Authorization", emptyToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({ 
      open: true, 
      feedbackCount: 3,
      applicationCount: 0,
      requestCount: 1
    });
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/metadata/adminstats');

    // Assert
    expect(res.statusCode).toEqual(401);
  });
});

describe("GET /metadata/dbstats", () => {
  beforeEach(async () => {
    await clearDb("application_db");
    await clearDb("application_request_db");
    await clearDb("feedback_db");
  });

  it("Correct data returned", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(3);
    await databases.getFeedbackDB().remove(dbData[0].id, dbData[0].rev);
    await saveRequestMockData(1);

    // Act
    const res = await request(app)
      .get(`/metadata/dbstats`)
      .set("Authorization", databaseToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      feedback_db: expect.objectContaining({ sizes: expect.any(Object), doc_del_count: 1, doc_count: 2 }),
      application_request_db: expect.objectContaining({ sizes: expect.any(Object), doc_del_count: 0, doc_count: 1 }),
      application_db: expect.objectContaining({ sizes: expect.any(Object), doc_del_count: 0, doc_count: 0 })
    });
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/metadata/dbstats');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/metadata/dbstats')
      .set('Authorization', updateOpenidToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /metadata/hourlystats", () => {
  beforeEach(async () => {
    await clearDb("application_db");
    await clearDb("application_request_db");
  });

  it("Correct data returned", async () => {
    // Arrange
    await saveApplicationMockData(3, [
      { datetime: "2021-12-06T09:39:42.404Z" },
      { datetime: "2023-12-06T09:51:42.404Z" },
      { datetime: "2021-12-06T12:39:42.404Z" }
    ]);
    await saveRequestMockData(3, [
      { datetime: "2021-11-06T21:39:42.404Z" },
      { datetime: "2021-12-06T03:39:42.404Z" },
      { datetime: "2021-12-07T09:01:42.404Z" }
    ]);

    // Act
    const res = await request(app)
      .get(`/metadata/hourlystats`)
      .set("Authorization", emptyToken);

    // Assert
    const data = Array.apply(null, Array(24)).map((_x, i) => { return { time: `${i}`, requestcount: 0, applicationcount: 0 } });
    data[5].requestcount = 1;
    data[11].requestcount = 1;
    data[11].applicationcount = 2;
    data[14].applicationcount = 1;
    data[23].requestcount = 1;

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(data);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/metadata/hourlystats');

    // Assert
    expect(res.statusCode).toEqual(401);
  });
});
const request = require("supertest")
const { mockSendMessage } = require("../../__mocks__/telegraf");

const { clearDb, getToken, stringGen, saveFeedbackMockData } = require("../_helpers");
const databases = require("../../src/databases.js");

const app = require('../../src/app')

let readToken, deleteToken, wrongToken;

beforeAll(async () => {
  readToken = await getToken("read:feedback");
  deleteToken = await getToken("delete:feedback");
  wrongToken = await getToken("read:applications");
});

beforeEach(() => {
  return clearDb("feedback_db");
});

describe("GET /feedback/:id", () => {
  it("Correct ID returns correct feedback", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);

    // Act
    const res = await request(app)
      .get(`/feedback/${dbData[0].id}`)
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(
      expect.objectContaining(dbData[0].doc)
    );
  });

  it("Incorrect ID returns 404", async () => {
    // Arrange
    await saveFeedbackMockData(1);

    // Act
    const res = await request(app)
      .get(`/feedback/wrong-id`)
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });
  
  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
    .get('/feedback/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/feedback/random-id')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /feedback", () => {
  it("Zero feedbacks", async () => {
    // Act
    const res = await request(app)
      .get('/feedback')
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).not.toBeNull();
    expect(res.body).toHaveLength(0);
  });

  it("One feedback", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(1);

    // Act
    const res = await request(app)
      .get('/feedback')
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0]).toEqual(
      expect.objectContaining(dbData[0].doc)
    );
  });

  it("Multiple feedbacks", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);

    // Act
    const res = await request(app)
      .get('/feedback')
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc)
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
      .get('/feedback');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get('/feedback')
      .set('Authorization', wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("DELETE /feedback/:id", () => {
  it("Correct document deleted", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);

    // Act
    const res = await request(app)
      .delete(`/feedback/${dbData[0].id}`)
      .set('Authorization', deleteToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Nothing deleted with wrong ID", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(2);

    // Act
    const res = await request(app)
      .delete(`/feedback/wrong-id`)
      .set('Authorization', deleteToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) }),
        expect.objectContaining({ doc: expect.objectContaining(dbData[1].doc) })
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
      .delete('/feedback/random-id');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .delete('/feedback/random-id')
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /feedback/delete", () => {
  it("Delete many existing feedbacks", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(3);

    // Act
    const res = await request(app)
      .post(`/feedback/delete`)
      .send({ ids: [dbData[0].id, dbData[2].id] })
      .set('Authorization', deleteToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[2].doc) })
      ])
    );
  });

  it("Delete many feedbacks but some not found", async () => {
    // Arrange
    let dbData = await saveFeedbackMockData(3);

    // Act
    const res = await request(app)
      .post(`/feedback/delete`)
      .send({ ids: [dbData[0].id, "random-id"] })
      .set('Authorization', deleteToken);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(dbData[0].doc) })
      ])
    );
  });

  it("Empty ID list returns 400", async () => {
    // Act
    const res = await request(app)
      .post('/feedback/delete')
      .send({ ids: [] })
      .set('Authorization', deleteToken);

    // Assert
    expect(res.statusCode).toEqual(400);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app)
      .post('/feedback/delete');

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .post('/feedback/delete')
      .set('Authorization', readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /feedback", () => {
  beforeEach(() => {
    mockSendMessage.mockClear();
  });

  it("Posting feedback with only text", async () => {
    let doc = {
      text: "testing text field"
    };

    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send(doc);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(doc) })
      ])
    );
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Posting feedback with maximum field lengths", async () => {
    let doc = {
      name: stringGen(40),
      text: stringGen(1500),
      email: stringGen(100),
      telegram: stringGen(50),
    };

    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send(doc);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ doc: expect.objectContaining(doc) })
      ])
    );
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Posting feedback with too long name", async () => {
    let doc = {
      name: stringGen(41),
      text: "Random text"
    };

    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send(doc);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting feedback with too long telegram", async () => {
    let doc = {
      telegram: stringGen(51),
      text: "Random text"
    };

    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send(doc);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting feedback with too long email", async () => {
    let doc = {
      telegram: stringGen(101),
      text: "Random text"
    };

    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send(doc);

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting empty feedback should return 400", async () => {
    // Act
    const res = await request(app)
      .post(`/feedback`)
      .send({});

    // Assert
    const newData = await databases.getFeedbackDB().allDocs({include_docs: true});

    expect(res.statusCode).toEqual(400);
    expect(newData.rows).toHaveLength(0);
    expect(mockSendMessage).not.toHaveBeenCalled();
  });
});
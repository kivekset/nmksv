const request = require("supertest");
const fs = require("fs");
const { mockSendMessage } = require("../../__mocks__/telegraf");
const { mockSendMail } = require("../../__mocks__/nodemailer");

const {
  clearDb,
  getToken,
  stringGen,
  updateApply,
  saveApplicationMockData,
  saveRequestMockData,
} = require("../_helpers");
const databases = require("../../src/databases.js");

const app = require("../../src/app");

let readToken, deleteToken, wrongToken;

beforeAll(async () => {
  readToken = await getToken("read:applications");
  deleteToken = await getToken("delete:applications");
  wrongToken = await getToken("read:applicationrequests");

  await clearDb("metadata_db");
});

beforeEach(() => {
  return clearDb("application_db");
});

describe("GET /applications/application/:id", () => {
  it("Valid request found", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    // Act
    const res = await request(app)
      .get(`/applications/application/${dbData[1].id}`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual(expect.objectContaining(dbData[1].doc));
  });

  it("Invalid ID returns 404", async () => {
    // Arrange
    await saveApplicationMockData(1);

    // Act
    const res = await request(app)
      .get(`/applications/application/wrong-id`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).get("/applications/application/random-id");

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get("/applications/application/random-id")
      .set("Authorization", wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("DELETE /applications/application/:id", () => {
  it("Correct document deleted", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    // Act
    const res = await request(app)
      .delete(`/applications/application/${dbData[1].id}`)
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Nothing deleted with wrong ID", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    // Act
    const res = await request(app)
      .delete(`/applications/application/wrong-id`)
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(404);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[0].doc),
        }),
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).delete(
      "/applications/application/random-id"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .delete("/applications/application/random-id")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /applications/application/attachment/:id/:name", () => {
  it("Get valid png attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.png",
        dbData[0].rev,
        file,
        "image/png"
      );

    // Act
    const res = await request(app)
      .get(
        `/applications/application/attachment/${dbData[0].id}/esisku_logo.png`
      )
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Get valid jpg attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.jpg"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.jpg",
        dbData[0].rev,
        file,
        "image/jpg"
      );

    // Act
    const res = await request(app)
      .get(
        `/applications/application/attachment/${dbData[0].id}/esisku_logo.jpg`
      )
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Get valid webp attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.webp"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.webp",
        dbData[0].rev,
        file,
        "image/webp"
      );

    // Act
    const res = await request(app)
      .get(
        `/applications/application/attachment/${dbData[0].id}/esisku_logo.webp`
      )
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Get valid avif attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.avif"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.avif",
        dbData[0].rev,
        file,
        "image/avif"
      );

    // Act
    const res = await request(app)
      .get(
        `/applications/application/attachment/${dbData[0].id}/esisku_logo.avif`
      )
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Get valid gif attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\testgif.gif"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "testgif.gif",
        dbData[0].rev,
        file,
        "image/gif"
      );

    // Act
    const res = await request(app)
      .get(`/applications/application/attachment/${dbData[0].id}/testgif.gif`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Get valid pdf attachment", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\tietosuojaseloste.pdf"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "tietosuojaseloste.pdf",
        dbData[0].rev,
        file,
        "application/pdf"
      );

    // Act
    const res = await request(app)
      .get(
        `/applications/application/attachment/${dbData[0].id}/tietosuojaseloste.pdf`
      )
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toBeInstanceOf(Buffer);
  });

  it("Incorrect ID returns 404", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.png",
        dbData[0].rev,
        file,
        "image/png"
      );

    // Act
    const res = await request(app)
      .get(`/applications/application/attachment/wrong-id/esisku_logo.png`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Incorrect attachment name returns 404", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    let file = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );
    await databases
      .getApplicationDB()
      .putAttachment(
        dbData[0].id,
        "esisku_logo.png",
        dbData[0].rev,
        file,
        "image/png"
      );

    // Act
    const res = await request(app)
      .get(`/applications/application/attachment/${dbData[0].id}/wrong-name`)
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(404);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).delete(
      "/applications/application/random-id"
    );

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .delete("/applications/application/random-id")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("GET /applications", () => {
  it("Zero applications", async () => {
    // Act
    const res = await request(app)
      .get("/applications")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).not.toBeNull();
    expect(res.body).toHaveLength(0);
  });

  it("One application", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(1);

    // Act
    const res = await request(app)
      .get("/applications")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(1);
    expect(res.body[0]).toEqual(expect.objectContaining(dbData[0].doc));
  });

  it("Many applications", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(2);

    // Act
    const res = await request(app)
      .get("/applications")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveLength(2);
    expect(res.body).toEqual(
      expect.arrayContaining([
        expect.objectContaining(dbData[0].doc),
        expect.objectContaining(dbData[1].doc),
      ])
    );
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).get("/applications");

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .get("/applications")
      .set("Authorization", wrongToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /applications/application/delete", () => {
  it("Delete many existing feedbacks", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(3);

    // Act
    const res = await request(app)
      .post(`/applications/application/delete`)
      .send({ ids: [dbData[1].id, dbData[2].id] })
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(1);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[2].doc),
        }),
      ])
    );
  });

  it("Delete many feedbacks but some not found", async () => {
    // Arrange
    let dbData = await saveApplicationMockData(3);

    // Act
    const res = await request(app)
      .post(`/applications/application/delete`)
      .send({ ids: [dbData[1].id, "wrong-id"] })
      .set("Authorization", deleteToken);

    // Assert
    const newData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(200);
    expect(newData.rows).toHaveLength(2);
    expect(newData.rows).toEqual(
      expect.not.arrayContaining([
        expect.objectContaining({
          doc: expect.objectContaining(dbData[1].doc),
        }),
      ])
    );
  });

  it("Empty ID list returns 400", async () => {
    // Act
    const res = await request(app)
      .post("/applications/application/delete")
      .send({ ids: [] })
      .set("Authorization", deleteToken);

    // Assert
    expect(res.statusCode).toEqual(400);
  });

  it("Unauthenticated without token", async () => {
    // Act
    const res = await request(app).post("/applications/application/delete");

    // Assert
    expect(res.statusCode).toEqual(401);
  });

  it("Unauthorized with wrong permission", async () => {
    // Act
    const res = await request(app)
      .post("/applications/application/delete")
      .set("Authorization", readToken);

    // Assert
    expect(res.statusCode).toEqual(403);
  });
});

describe("POST /applications/application", () => {
  beforeEach(() => {
    mockSendMail.mockClear();
    mockSendMessage.mockClear();

    return clearDb("application_request_db");
  });

  it("Minimal valid application should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Maximum valid application should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );
    let file2 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.jpg"
    );
    let file3 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\tietosuojaseloste.pdf"
    );

    let doc = {
      requestId: reqData[0].id,
      name: stringGen(40),
      email: stringGen(92) + "@tuni.fi",
      startYear: "2022",
      studyField: stringGen(40),
      activityCV: stringGen(500),
      freeWord: stringGen(500),
      reasonsToApply: stringGen(500),
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .field("activityCV", doc.activityCV)
      .field("freeWord", doc.freeWord)
      .field("reasonsToApply", doc.reasonsToApply)
      .attach("attachments", file1, {
        filename: "esisku_logo.png",
        contentType: "image/png",
      })
      .attach("attachments", file2, {
        filename: "esisku_logo.jpg",
        contentType: "image/jpg",
      })
      .attach("attachments", file3, {
        filename: "tietosuojaseloste.pdf",
        contentType: "application/pdf",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "esisku_logo.png": expect.any(Object),
          "esisku_logo.jpg": expect.any(Object),
          "tietosuojaseloste.pdf": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application without request ID should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with incorrect request ID should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: "wrong-id",
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(404);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application without name should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long name should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: stringGen(41),
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application without email should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long email should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: stringGen(93) + "@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application without start year should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with invalid start year should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "ny",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application without study field should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long study field should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: stringGen(41),
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long activity CV should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
      activityCV: stringGen(501),
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long free word should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
      freeWord: stringGen(501),
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too long reasons to apply should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
      reasonsToApply: stringGen(501),
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with other than @tuni.fi email should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@wrong.email",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with png attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "esisku_logo.png",
        contentType: "image/png",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "esisku_logo.png": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with jpg attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.jpg"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "esisku_logo.jpg",
        contentType: "image/jpg",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "esisku_logo.jpg": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with avif attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.avif"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "esisku_logo.avif",
        contentType: "image/avif",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "esisku_logo.avif": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with webp attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.webp"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "esisku_logo.webp",
        contentType: "image/webp",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "esisku_logo.webp": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with gif attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\testgif.gif"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "testgif.gif",
        contentType: "image/gif",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "testgif.gif": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with pdf attachment should success", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\tietosuojaseloste.pdf"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "tietosuojaseloste.pdf",
        contentType: "application/pdf",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
        _attachments: {
          "tietosuojaseloste.pdf": expect.any(Object),
        },
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Application with wrong type attachment should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\wrong_type.txt"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "wrong_type.txt",
        contentType: "text/plain",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(415);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with renamed wrong type attachment should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\wrong_type_renamed.png"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "wrong_type_renamed.png",
        contentType: "image/png",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(415);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too big attachment should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\too_big.jpg"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "too_big.jpg",
        contentType: "image/jpg",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(413);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Application with too many attachments should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);

    let file1 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.png"
    );
    let file2 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.jpg"
    );
    let file3 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.webp"
    );
    let file4 = fs.readFileSync(
      __dirname + "\\..\\data\\attachments\\esisku_logo.avif"
    );

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app)
      .post(`/applications/application`)
      .field("requestId", doc.requestId)
      .field("name", doc.name)
      .field("email", doc.email)
      .field("startYear", doc.startYear)
      .field("studyField", doc.studyField)
      .attach("attachments", file1, {
        filename: "esisku_logo.png",
        contentType: "image/png",
      })
      .attach("attachments", file2, {
        filename: "esisku_logo.jpg",
        contentType: "image/jpg",
      })
      .attach("attachments", file3, {
        filename: "esisku_logo.webp",
        contentType: "image/webp",
      })
      .attach("attachments", file4, {
        filename: "esisku_logo.avif",
        contentType: "image/avif",
      });

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(400);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting when apply is not open should fail", async () => {
    // Arrange
    await updateApply(false);
    let reqData = await saveRequestMockData(1);

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(403);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting when wait time is not passed should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1, [
      { datetime: new Date(new Date() - 120 * 60 * 1000).toISOString() },
    ]); // -2h

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(403);

    expect(newApplicationData.rows).toHaveLength(0);
    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: false,
        applicationId: "",
      })
    );

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });

  it("Posting when wait time is bypassed should succeed", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1, [
      {
        datetime: new Date(new Date() - 120 * 60 * 1000).toISOString(),
        bypassWaitTime: true,
      },
    ]); // -2h

    let doc = {
      requestId: reqData[0].id,
      name: "Ville Vallaton",
      email: "ville@tuni.fi",
      startYear: "2022",
      studyField: "sähkö",
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newRequestData = await databases
      .getRequestDB()
      .allDocs({ include_docs: true });
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });
    const newApplicationId = newApplicationData.rows.find(
      (x) => x.doc.requestId === doc.requestId
    ).id;

    expect(res.statusCode).toEqual(200);

    expect(newApplicationData.rows).toHaveLength(1);
    expect(newApplicationData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...doc,
        emailSent: true,
        _id: newApplicationId,
      })
    );

    expect(newRequestData.rows[0].doc).toEqual(
      expect.objectContaining({
        ...reqData[0].doc,
        applicationSent: true,
        applicationId: newApplicationId,
      })
    );

    expect(mockSendMail).toHaveBeenCalledTimes(2);
    expect(mockSendMessage).toHaveBeenCalledTimes(1);
  });

  it("Posting application that has already been posted should fail", async () => {
    // Arrange
    await updateApply(true);
    let reqData = await saveRequestMockData(1);
    let appData = await saveApplicationMockData(1, [
      {
        requestId: reqData[0].id,
        name: reqData[0].doc.name,
        email: reqData[0].doc.email,
      },
    ]);
    await databases.getRequestDB().put({
      ...reqData[0].doc,
      _id: reqData[0].id,
      _rev: reqData[0].rev,
      applicationSent: true,
      applicationId: appData[0].id,
    });

    let doc = {
      requestId: reqData[0].id,
      name: reqData[0].doc.name,
      email: reqData[0].doc.email,
      startYear: appData[0].doc.startYear,
      studyField: appData[0].doc.studyField,
    };

    // Act
    const res = await request(app).post(`/applications/application`).send(doc);

    // Assert
    const newApplicationData = await databases
      .getApplicationDB()
      .allDocs({ include_docs: true });

    expect(res.statusCode).toEqual(403);

    expect(newApplicationData.rows).toHaveLength(1);

    expect(mockSendMail).not.toHaveBeenCalled();
    expect(mockSendMessage).not.toHaveBeenCalled();
  });
});

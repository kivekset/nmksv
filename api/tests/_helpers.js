const PouchDB = require("pouchdb");
const axios = require("axios").default;
const fetch = require("node-fetch");
require("dotenv").config({path:__dirname + '/./../.env.development.local'});

const databases = require("../src/databases.js");

const feedbackData = require("./data/feedback.json");
const requestData = require("./data/request.json");
const applicationData = require("./data/application.json");
const updateData = require("./data/applyOpenUpdates.json");

/**
 * Clears database from all documents
 * @param {String} db Database name to be cleared
 * @returns Empty promise 
 */
const clearDb = db => {
  return new Promise((resolve, reject) => {
    new PouchDB(`${databases.getAdminBaseUrl()}${db}`).destroy()
    .then(() => {
      return new PouchDB(`${databases.getAdminBaseUrl()}${db}`).info()
    })
    .then(() => {
      const postData = JSON.stringify({
        "admins": {
          "names": [],
          "roles": []
        },
        "members": {
          "names": [process.env.DB_USER],
          "roles": []
        }
      });
  
      return fetch(databases.getAdminBaseUrl() + db + "/_security", {
        method: "PUT",
        body: postData,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        }
      })
    })
    .then(() => resolve())
    .catch(err => {
      reject(err)
    });
  })
}

/**
 * Gets auth0 token for test user
 * @param {String} scope Scope to retrieve
 * @returns {Promise<String>} Promise with scheme and token
 */
const getToken = scope => {
  let options = {
    method: 'POST',
    url: process.env.AUTH_ISSUER + "oauth/token",
    data: {
      grant_type: 'password',
      username: process.env.TEST_AUTH_USER,
      password: process.env.TEST_AUTH_PASS,
      audience: process.env.AUTH_AUDIENCE,
      client_id: process.env.AUTH_CLIENT_ID,
      client_secret: process.env.AUTH_CLIENT_SECRET
    }
  };

  if (scope) options.data.scope = scope;
  
  return new Promise((resolve, reject) => {
    axios.request(options)
    .then(response => {
      if (!response.data.access_token) return reject();
      resolve("Bearer " + response.data.access_token);
    }).catch(error => {
      reject(error);
    });
  });
}

/**
 * Generate random string
 * @param {Number} len Length of the string to be generated
 * @returns Generated string
 */
const stringGen = len => {
  let text = "";
  const charset = "abcdefghijklmnopqrstuvwxyz0123456789";
  
  for (let i = 0; i < len; ++i)
    text += charset.charAt(Math.floor(Math.random() * charset.length));
  
  return text;
}

/**
 * Updates apply open metadata
 * @param {Boolean} open Boolean for apply status
 * @param {Number} updates Number of updates to put
 * @returns Empty promise
 */
const updateApply = async (open, updates) => {
  if (updates > updateData.length) throw new Error("Too many updates requested with param 'updates'");

  let apply;
  try {
    apply = await databases.getMetadataDB().get("applyMetaData");
  } catch {}

  let doc = {
    _id: "applyMetaData",
    isManuallyOpen: open,
    applyTimes: [],
    updates: (
      updates
      ? updateData
        .slice(0, updates)
        .map((e, i) => {return { ...e, newState: i%2 ? !open : open, previousState: i%2 ? open : !open };})
      : undefined
    )
  };
  if (apply?._rev) doc._rev = apply._rev;
  let putData = await databases.getMetadataDB().put(doc);

  return { ...doc, _rev: putData.rev };
};

/**
 * Saves valid mock feedback data into database
 * @param {Number} n How many feedbacks to save
 * @param {Array<Object>} overrideData Array of objects to override partially or fully the default mock data
 * @returns {Promise<Object[]>} Saved data with format { id: _id, rev: _rev, doc: { saved document } }
 */
const saveFeedbackMockData = async (n, overrideData) => {
  if (n > feedbackData.length) throw new Error("Too many feedbacks requested with param 'n'");

  let data = await Promise.all(
    (n ? feedbackData.slice(0, n) : feedbackData)
      .map(async (data, i) => {
        let override = { ...data, ...(overrideData?.[i]) };
        let postData = await databases.getFeedbackDB().post(override);
        return { ...postData, doc: override };
      })
  );

  return data;
};

/**
 * Saves valid mock application request data into database
 * @param {Number} n How many requests to save
 * @param {Array<Object>} overrideData Array of objects to override partially or fully the default mock data
 * @returns {Promise<Object[]>} Saved data with format { id: _id, rev: _rev, doc: { saved document } }
 */
const saveRequestMockData = async (n, overrideData) => {
  if (n > requestData.length) throw new Error("Too many application requests requested with param 'n'");

  let data = await Promise.all(
    (n ? requestData.slice(0, n) : requestData)
      .map(async (data, i) => {
        let override = { ...data, ...(overrideData?.[i]) };
        let postData = await databases.getRequestDB().post(override);
        return { ...postData, doc: override };
      })
  );

  return data;
};

/**
 * Saves valid mock application data into database
 * @param {Number} n How many applications to save
 * @param {Array<Object>} overrideData Array of objects to override partially or fully the default mock data
 * @returns {Promise<Object[]>} Saved data with format { id: _id, rev: _rev, doc: { saved document } }
 */
const saveApplicationMockData = async (n, overrideData) => {
  if (n > applicationData.length) throw new Error("Too many applications requested with param 'n'");

  let data = await Promise.all(
    (n ? applicationData.slice(0, n) : applicationData)
      .map(async (data, i) => {
        let override = { ...data, ...(overrideData?.[i]) };
        let postData = await databases.getApplicationDB().post(override);
        return { ...postData, doc: override };
      })
  );

  return data;
};

module.exports = {
  clearDb,
  getToken,
  stringGen,
  updateApply,
  saveFeedbackMockData,
  saveRequestMockData,
  saveApplicationMockData
};
const nodemailer = jest.createMockFromModule('nodemailer');

const mockSendMail = jest.fn((_mail, callback) => callback());
nodemailer.createTransport.mockReturnValue({
  sendMail: mockSendMail,
  use: jest.fn(),
  verify: jest.fn(() => Promise.resolve())
});

const myModule = module.exports = nodemailer;
myModule.mockSendMail = mockSendMail;
const mockSendMessage = jest.fn(() => Promise.resolve());
const Telegraf = jest.fn().mockImplementation(() => {
  return {
    launch: jest.fn(() => Promise.resolve()),
    telegram: {
      sendMessage: mockSendMessage
    }
  };
});

module.exports = {
  Telegraf,
  mockSendMessage
};
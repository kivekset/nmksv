const fetch = require("node-fetch");
const databases = require("../databases");
const { auth } = require("express-oauth2-jwt-bearer");
const { validationResult } = require("express-validator");

const checkJwt = auth({
  audience: process.env.AUTH_AUDIENCE,
  issuerBaseURL: process.env.AUTH_ISSUER,
});

const getUserInfo = async (req, res, next) => {
  fetch(process.env.AUTH_ISSUER + "userinfo", {
    method: "GET",
    headers: {
      Authorization: `Bearer ${req.auth.token}`,
    },
  })
    .then((data) => data.json())
    .then((data) => {
      req.user = {
        email: data.email,
        name: data.name,
      };
      next();
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

// Is apply open function
const isApplyOpen = async () => {
  return new Promise((resolve, reject) => {
    databases
      .getMetadataDB()
      .get("applyMetaData")
      .then((doc) => {
        return resolve(doc.isManuallyOpen);
      })
      .catch((err) => {
        return reject(err);
      });
  });
};

const validRequest = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  return next();
};

module.exports = {
  isApplyOpen,
  checkJwt,
  getUserInfo,
  validRequest,
};

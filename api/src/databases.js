const PouchDB = require("pouchdb");
PouchDB.plugin(require("pouchdb-find"));

const db_user = process.env.DB_USER;
const db_password = process.env.DB_PASSWORD;
const db_url = process.env.NODE_ENV === "test" ? process.env.TEST_DB_URL : process.env.DB_URL;
const db_admin_user = process.env.DB_ADMIN_USER;
const db_admin_pass = process.env.DB_ADMIN_PASS;

let request_db;
let application_db;
let metadata_db;
let feedback_db;

const refresh = () => {
  request_db = new PouchDB(
    `http://${db_user}:${db_password}@${db_url}/application_request_db`
  );
  
  application_db = new PouchDB(
    `http://${db_user}:${db_password}@${db_url}/application_db`
  );
  
  metadata_db = new PouchDB(
    `http://${db_user}:${db_password}@${db_url}/metadata_db`
  );
  
  feedback_db = new PouchDB(
    `http://${db_user}:${db_password}@${db_url}/feedback_db`
  );
}

refresh();

const getRequestDB = () => {
  return request_db;
};

const getApplicationDB = () => {
  return application_db;
};

const getMetadataDB = () => {
  return metadata_db;
};

const getFeedbackDB = () => {
  return feedback_db;
};

const getBaseUrl = () => { return `http://${db_user}:${db_password}@${db_url}/`; }
const getAdminBaseUrl = () => { return `http://${db_admin_user}:${db_admin_pass}@${db_url}/`; }

module.exports.refresh = refresh;
module.exports.getBaseUrl = getBaseUrl;
module.exports.getAdminBaseUrl = getAdminBaseUrl;
module.exports.getRequestDB = getRequestDB;
module.exports.getApplicationDB = getApplicationDB;
module.exports.getMetadataDB = getMetadataDB;
module.exports.getFeedbackDB = getFeedbackDB;

const express = require("express");
const cors = require("cors");

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config({
    path: __dirname + "/./../.env.development.local",
  });
}

const BaseRouter = require("./routes");
const emailer = require("./services/emailService.js");
require("./services/telegramService");

const app = express();

const corsConfig = {
  origin: process.env.CORS_ORIGIN
};

app.use(cors(corsConfig));

app.use(express.json({ limit: "15mb" }));

// Initialize email service
emailer.initialize();

// Enable CORS pre-flight
app.options("*", cors());

app.use("/", BaseRouter);

module.exports = app;

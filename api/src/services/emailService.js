const nodemailer = require("nodemailer");
const nodeexphbs = require("nodemailer-express-handlebars");
const databases = require("../databases.js");

var transporter;

const initialize = () => {
  // Nodemailer setup
  var mailCFG;
  if (process.env.NODE_ENV === "production") {
    mailCFG = {
      host: "localhost",
      port: 25,
      tls: {
        rejectUnauthorized: false
      }
    };
  } else {
    mailCFG = {
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.NODEMAILER_EMAIL,
        pass: process.env.NODEMAILER_PASS
      }
    }
  };

  // Create transporter
  transporter = nodemailer.createTransport(mailCFG);

  // Configure email template engine
  const options = {
    viewEngine: {
      extname: ".hbs",
      layoutsDir: process.cwd() + "/src/templates",
      defaultLayout: "main",
    },
    viewPath: process.cwd() + "/src/templates",
    extName: ".hbs"
  };

  transporter.use("compile", nodeexphbs(options));

  transporter.verify().catch(err => console.log(err));
};

const getTransporter = () => {
  if (!transporter) initialize();
  return transporter;
};

/**
 * Send our request email to the requester.
 * Updates also the request DB object to be emailSent = true if successful
 * @param {*} doc Request DB document 
 */
const sendRequestEmail = doc => {
  return new Promise((resolve, reject) => {
    var mailOne = {
      from: process.env.NODEMAILER_EMAIL,
      to: doc.email,
      subject: "NMKSV hakemuslinkki",
      template: "request_email",
      context: {
        name: doc.name,
        id: doc._id
      }
    };

    transporter.sendMail(mailOne, err => {
      if (err) {
        return reject(err);
      }

      // Update request to be sent
      databases
      .getRequestDB()
      .put({
        ...doc,
        emailSent: true
      })
      .then(() => {
        return resolve();
      })
      .catch(err => {
        return reject(err);
      })
    });
  });
};

/**
 * Send our application email to us and applicant
 * Updates also the application DB object to be emailSent = true if successful
 * @param {*} doc Application DB document 
 */
const sendApplicationEmail = doc => {
  return new Promise((resolve, reject) => {

    // Create attachments
    var attachments = [];

    for (const key in doc._attachments) {
      var attachment = {
        filename: key,
        content: Buffer.from(doc._attachments[key].data, "base64")
      };

      attachments.push(attachment);
    }

    // Send mail back to the applicant
    var mailTwo = {
      from: process.env.NODEMAILER_EMAIL,
      to: doc.email,
      subject: "NMKSV hakemus",
      template: "application_email",
      attachments: attachments,
      context: {
        ...doc
      }
    };

    transporter.sendMail(mailTwo, err => {
      if (err) {
        return reject(err);
      }

      // Send mail to us
      var mailThree = {
        from: process.env.NODEMAILER_EMAIL,
        to: "careers@nmksv.org",
        subject: "NMKSV hakemus",
        template: "application_email",
        attachments: attachments,
        context: {
          ...doc
        }
      };

      transporter.sendMail(mailThree, err => {
        if (err) {
          return reject(err);
        }
  
        // Update application to be sent
        databases
        .getApplicationDB()
        .put({
          ...doc,
          emailSent: true
        })
        .then(() => {
          return resolve();
        })
        .catch(err => {
          return reject(err);
        });
      });
    });
  });
};

module.exports= {
  initialize,
  getTransporter,
  sendRequestEmail,
  sendApplicationEmail
};
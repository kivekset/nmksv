/**
 * At this moment the bot sends messages to only one pre-defined chat.
 * Chat is defined by the chat id.
 */

const { Telegraf } = require("telegraf");

const bot = new Telegraf(process.env.TELEGRAM_TOKEN);
bot.launch().catch((err) => console.log(err));

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));

let channel_id =
  process.env.NODE_ENV === "production"
    ? process.env.TELEGRAM_CHANNEL_ID_PROD
    : process.env.TELEGRAM_CHANNEL_ID_DEV;

const sendMessage = (text) => {
  return bot.telegram.sendMessage(channel_id, text, {
    disable_web_page_preview: true,
    protect_content: true,
  });
};

module.exports = {
  sendMessage,
};

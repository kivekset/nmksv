const express = require("express");
const { body } = require("express-validator");
const databases = require("../databases");
const {
  getOne,
  getAll,
  deleteOne,
  deleteMany,
} = require("../controllers/databaseFunctions");
const telegramBot = require("../services/telegramService");

const { requiredScopes } = require("express-oauth2-jwt-bearer");
const checkReadScope = requiredScopes("read:feedback");
const checkDeleteScope = requiredScopes("delete:feedback");

const { checkJwt, validRequest } = require("../utils/helperFunctions");

const router = express.Router();

const feedbackStrings = [
  "Saitte palautetta.",
  "PALAUTETTA",
  "NYT JYSÄHTI ... palautetta",
  "https://www.youtube.com/watch?v=dQw4w9WgXcQ ps. palautetta",
  "Nii sainki palautetta mitä sitte",
  "Palautepostia",
  "Varppina pistin palautetta",
  "palautetta päshähti",
];

const getRandomFeedbackString = () => {
  return feedbackStrings[Math.floor(Math.random() * feedbackStrings.length)];
};

/* Post new feedback */
router.post(
  "/",
  [
    body("text").not().isEmpty(),
    body("text").isLength({ max: 1500 }),
    body("name").optional(),
    body("name").isLength({ max: 40 }),
    body("name").optional(),
    body("email").isLength({ max: 100 }),
    body("email").optional(),
    body("telegram").isLength({ max: 50 }),
  ],
  validRequest,
  async (req, res) => {
    let doc = {
      name: req.body.name,
      text: req.body.text,
      email: req.body.email,
      telegram: req.body.telegram,
      datetime: new Date().toISOString(),
    };

    databases
      .getFeedbackDB()
      .post(doc)
      .then(() => {
        return telegramBot.sendMessage(getRandomFeedbackString());
      })
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

/* Get one feedback */
router.get("/:id", checkJwt, checkReadScope, async (req, res) =>
  getOne(req.params.id, res, databases.getFeedbackDB())
);

/* Get all feedbacks */
router.get("/", checkJwt, checkReadScope, async (_req, res) =>
  getAll(res, databases.getFeedbackDB())
);

/* Delete one feedback */
router.delete("/:id", checkJwt, checkDeleteScope, async (req, res) =>
  deleteOne(req.params.id, res, databases.getFeedbackDB())
);

/* Delete many feedbacks */
router.post(
  "/delete",
  [body("ids").not().isEmpty()],
  checkJwt,
  checkDeleteScope,
  validRequest,
  async (req, res) => deleteMany(req.body.ids, res, databases.getFeedbackDB())
);

module.exports = router;

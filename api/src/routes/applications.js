const express = require("express");
const multer = require("multer");
const FileType = require("file-type");
const { body } = require("express-validator");
const databases = require("../databases");
const {
  getOne,
  getAll,
  deleteOne,
  deleteMany,
} = require("../controllers/databaseFunctions");
const { sendApplicationEmail } = require("../services/emailService.js");
const telegramBot = require("../services/telegramService");

const { requiredScopes } = require("express-oauth2-jwt-bearer");
const checkReadScope = requiredScopes("read:applications");
const checkDeleteScope = requiredScopes("delete:applications");

const router = express.Router();

const whitelist = [
  "image/png",
  "image/jpeg",
  "image/jpg",
  "image/gif",
  "image/webp",
  "image/avif",
  "application/pdf",
];

// Load files into request (program memory) as buffer
const storage = multer.memoryStorage();
const upload = multer({
  storage: storage,
  limits: { fileSize: 3000000 }, // 3MB
  fileFilter: (_req, file, cb) => {
    if (!whitelist.includes(file.mimetype)) {
      return cb(new multer.MulterError("LIMIT_FIELD_VALUE"));
    }

    cb(null, true);
  },
});

const {
  checkJwt,
  isApplyOpen,
  validRequest,
} = require("../utils/helperFunctions");

// Wait time for application to open
let HOUR_LIMIT = 12;

/* Get single application for admin view */
router.get("/application/:id", checkJwt, checkReadScope, async (req, res) =>
  getOne(req.params.id, res, databases.getApplicationDB(), {
    attachments: true,
  })
);

/* Get one attachment in an application */
router.get(
  "/application/attachment/:id/:name",
  checkJwt,
  checkReadScope,
  async (req, res) => {
    databases
      .getApplicationDB()
      .getAttachment(req.params.id, req.params.name)
      .then((buffer) => {
        return res.type(buffer.type).send(buffer);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

/* Get all applications in database */
router.get("/", checkJwt, checkReadScope, async (_req, res) =>
  getAll(res, databases.getApplicationDB(), { attachments: true })
);

/* Delete one application */
router.delete(
  "/application/:id",
  checkJwt,
  checkDeleteScope,
  async (req, res) =>
    deleteOne(req.params.id, res, databases.getApplicationDB())
);

/* Delete many applications */
router.post(
  "/application/delete",
  [body("ids").not().isEmpty()],
  checkJwt,
  checkDeleteScope,
  validRequest,
  async (req, res) =>
    deleteMany(req.body.ids, res, databases.getApplicationDB())
);

/* Post new application */
const multerErrorHandler = (err, _req, res, next) => {
  if (err) {
    if (err.code === "LIMIT_FILE_SIZE")
      return res.status(413).json({ errors: ["Liian suuri tiedosto."] });

    if (err.code === "LIMIT_FIELD_VALUE")
      return res
        .status(415)
        .json({ errors: ["Virheellinen tiedostoformaatti."] });

    return res.status(400).json({ errors: ["Virhe tiedoston käsittelyssä."] });
  } else next();
};

router.post(
  "/application",
  upload.array("attachments", 3),
  multerErrorHandler,
  [
    body("requestId").not().isEmpty(),
    body("name", "Nimi ei saa olla tyhjä.").not().isEmpty(),
    body("name", "Nimen maksimipituus on 40 merkkiä.").isLength({ max: 40 }),
    body("email", "Sähköpostin tulee olla @tuni.fi osoite.").isEmail({
      ignore_max_length: true,
      host_whitelist: ["tuni.fi"],
    }),
    body("email", "Sähköpostin maksimipituus on 100 merkkiä.").isLength({
      max: 100,
    }),
    body("startYear", "Aloitusvuosi ei saa olla tyhjä.").not().isEmpty(),
    body("startYear", "Aloitusvuoden tulee olla nelinumeroinen.")
      .isLength({ max: 4, min: 4 })
      .isInt(),
    body("studyField", "Opiskeluala ei saa olla tyhjä.").not().isEmpty(),
    body("studyField", "Opiskelualan maksimipituus on 40 merkkiä.").isLength({
      max: 40,
    }),
    body("activityCV").optional(),
    body(
      "activityCV",
      "Järjestötoiminna CV saa olla enintään 500 merkkiä."
    ).isLength({ max: 500 }),
    body("freeWord").optional(),
    body("freeWord", "Oma esittely saa olla enintään 500 merkkiä.").isLength({
      max: 500,
    }),
    body("reasonsToApply").optional(),
    body(
      "reasonsToApply",
      "Mukaan haluamisen teksti saa olla enintään 500 merkkiä."
    ).isLength({ max: 500 }),
  ],
  validRequest,
  async (req, res) => {
    // Check that the apply is open
    const isOpen = await isApplyOpen();
    if (!isOpen)
      return res.status(403).json({ errors: ["Hakeminen ei ole auki."] });

    let application = {
      requestId: req.body.requestId,
      name: req.body.name,
      email: req.body.email,
      datetime: new Date().toISOString(),
      startYear: req.body.startYear,
      studyField: req.body.studyField,
      activityCV: req.body.activityCV,
      freeWord: req.body.freeWord,
      reasonsToApply: req.body.reasonsToApply,
      emailSent: false,
    };

    if (req.files && req.files.length > 0) {
      // Modify files object to correct format for couch db
      let attachments = {};
      for (let file of req.files) {
        const meta = await FileType.fromBuffer(file.buffer);
        if (!meta || !whitelist.includes(meta.mime))
          return res
            .status(415)
            .json({ errors: ["Tiedostomuoto ei ole sallittu."] });

        attachments[file.originalname] = {
          content_type: file.mimetype,
          data: file.buffer,
        };
      }

      application._attachments = attachments;
    }

    let reqDoc, sendDoc;
    // First fetch the application request
    databases
      .getRequestDB()
      .get(req.body.requestId)
      .then((doc) => {
        // Save request document for later
        reqDoc = doc;

        // Application already sent
        if (doc.applicationSent)
          return Promise.reject({
            status: 403,
            message: "Hakemuspyyntöön liittyvä hakemus on jo lähetetty.",
          });

        // Check for wait time
        let requestDate = Date.parse(doc.datetime);
        let diff = Math.abs(Date.now() - requestDate);
        let hours = Math.floor(diff / (1000 * 60 * 60));

        // Not enough time waited yet
        if (!doc.bypassWaitTime && hours < HOUR_LIMIT)
          return Promise.reject({
            status: 403,
            message: "Odotusaika ei ole vielä kulunut",
          });

        // Save application
        return databases.getApplicationDB().post(application);
      })
      .then((postDoc) => {
        sendDoc = postDoc;
        return databases
          .getApplicationDB()
          .get(postDoc.id, { attachments: true });
      })
      .then((sDoc) => {
        return sendApplicationEmail(sDoc);
      })
      .then(() => {
        // Update application request
        let updatedDoc = {
          ...reqDoc,
          applicationSent: true,
          applicationId: sendDoc.id,
        };

        return databases.getRequestDB().put(updatedDoc);
      })
      .then(() => {
        return telegramBot.sendMessage("Uusi hakemus!");
      })
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

module.exports = router;

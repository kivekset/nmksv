const express = require("express");
const applications = require("./applications");
const requests = require("./requests");
const metadata = require("./metadata");
const feedback = require("./feedback");
const dbadmin = require("./dbadmin");
const deleted = require("./deleted");

// Init router and path
const router = express.Router();

// Add sub-routes
router.use("/applications", applications);
router.use("/applications", requests);
router.use("/metadata", metadata);
router.use("/feedback", feedback);
router.use("/dbadmin", dbadmin);
router.use("/deleted", deleted);

module.exports = router;

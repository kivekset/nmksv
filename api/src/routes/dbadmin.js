const express = require("express");
const fetch = require("node-fetch");
const PouchDB = require("pouchdb");
const { param } = require("express-validator");
const databases = require("../databases");

const { requiredScopes } = require("express-oauth2-jwt-bearer");
const checkDatabaseScope = requiredScopes("all:database");

const router = express.Router();

const { checkJwt, validRequest } = require("../utils/helperFunctions");

// RPC style purge of database
// Permanently deletes documents that have been removed
router.post(
  "/purge/:db",
  [param("db").not().isEmpty()],
  checkJwt,
  checkDatabaseScope,
  validRequest,
  async (req, res) => {
    let dbName;
    if (req.params.db === "applications") {
      dbName = "application_db";
    } else if (req.params.db === "applicationrequests") {
      dbName = "application_request_db";
    } else if (req.params.db === "feedbacks") {
      dbName = "feedback_db";
    } else {
      return res.sendStatus(404);
    }

    const dbinst = new PouchDB(databases.getAdminBaseUrl() + dbName);
    const tmpDBName = `tmp${dbName}`;

    // Destroy possible old temporary database
    new PouchDB(tmpDBName)
      .destroy()

      // Create temporary database
      .then(() => Promise.resolve(new PouchDB(tmpDBName)))

      // Replicate original database to temporary database with only undeleted documents
      .then(
        (tmpDB) =>
          new Promise((resolve, reject) => {
            dbinst.replicate
              .to(tmpDB, { filter: (doc) => !doc._deleted })
              .on("complete", () => resolve(tmpDB))
              .on("denied", reject)
              .on("error", reject);
          })
      )

      // Destroy the original database
      .then((tmpDB) => {
        return dbinst
          .destroy()
          .then(() => Promise.resolve(tmpDB))
          .catch((err) => Promise.reject(err));
      })

      // Create the original database again
      .then(
        (tmpDB) =>
          new Promise((resolve, reject) => {
            try {
              resolve({
                db: new PouchDB(`${databases.getAdminBaseUrl()}${dbName}`),
                tmpDB: tmpDB,
              });
            } catch (e) {
              reject(e);
            }
          })
      )

      // Replicate the temporary database to new original database
      .then(
        ({ db, tmpDB }) =>
          new Promise((resolve, reject) => {
            tmpDB.replicate
              .to(db)
              .on("complete", () => resolve(tmpDB))
              .on("denied", reject)
              .on("error", reject);
          })
      )

      // Destroy the temporary database
      .then((tmpDB) => tmpDB.destroy())
      .then(() => {
        return new Promise((resolve, reject) => {
          const postData = JSON.stringify({
            admins: {
              names: [],
              roles: [],
            },
            members: {
              names: [process.env.DB_USER],
              roles: [],
            },
          });

          fetch(databases.getAdminBaseUrl() + dbName + "/_security", {
            method: "PUT",
            body: postData,
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
          })
            .then((response) => response.json())
            .then((data) => (data?.ok ? resolve() : reject(data)))
            .catch((err) => reject(err));
        });
      })
      .then(() => {
        return res.status(200).json({ message: "Cleanup complete" });
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

module.exports = router;

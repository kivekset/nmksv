const express = require("express");
const { body } = require("express-validator");
const databases = require("../databases");
const telegramBot = require("../services/telegramService");

const { requiredScopes } = require("express-oauth2-jwt-bearer");

const checkUpdateScope = requiredScopes("update:applyopen");
const checkOpenIdScope = requiredScopes("openid");
const checkDatabaseScope = requiredScopes("all:database");

const router = express.Router();

const {
  checkJwt,
  isApplyOpen,
  validRequest,
  getUserInfo,
} = require("../utils/helperFunctions");
const { getOne } = require("../controllers/databaseFunctions");

const APPLYOPEN_HISTORY_COUNT = 10;

/* Get boolean for is apply open */
router.get("/applyOpenPublic", async (_req, res) => {
  databases
    .getMetadataDB()
    .get("applyMetaData")
    .then((data) => {
      return res.send(data.isManuallyOpen);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
});

/* Get apply open data */
router.get("/applyOpen", checkJwt, async (_req, res) =>
  getOne("applyMetaData", res, databases.getMetadataDB())
);

/* Put apply open */
router.put(
  "/applyOpen",
  [body("open").isBoolean()],
  checkJwt,
  checkUpdateScope,
  checkOpenIdScope,
  getUserInfo,
  validRequest,
  async (req, res) => {
    databases
      .getMetadataDB()
      .get("applyMetaData")
      .then((doc) => {
        let newUpdate = {
          name: req.user.name,
          email: req.user.email,
          datetime: new Date().toISOString(),
          previousState: doc.isManuallyOpen,
          newState: req.body.open,
        };

        let updates = doc.updates ?? [];
        updates.sort((a, b) => new Date(b.datetime) - new Date(a.datetime));
        while (updates.length >= APPLYOPEN_HISTORY_COUNT) updates.pop();
        updates.unshift(newUpdate);

        return databases.getMetadataDB().put({
          _id: "applyMetaData",
          _rev: doc._rev,
          isManuallyOpen: req.body.open,
          applyTimes: doc.applyTimes,
          updates: updates,
        });
      })
      .then(() => {
        return telegramBot.sendMessage(
          `Haku ${req.body.open ? "avattu" : "suljettu"}`
        );
      })
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

/* Get general statistics */
router.get("/adminStats", checkJwt, async (req, res) => {
  let stats = {};
  isApplyOpen()
    .then((result) => {
      stats.open = result;
      return databases.getFeedbackDB().info();
    })
    .then((feedbackInfo) => {
      stats.feedbackCount = feedbackInfo.doc_count;
      return databases.getApplicationDB().info();
    })
    .then((applicationInfo) => {
      stats.applicationCount = applicationInfo.doc_count;
      return databases.getRequestDB().info();
    })
    .then((requestInfo) => {
      stats.requestCount = requestInfo.doc_count;
      return res.json(stats);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
});

/* Get database statistics */
router.get("/dbstats", checkJwt, checkDatabaseScope, async (_req, res) => {
  let stats = {};
  databases
    .getApplicationDB()
    .info()
    .then((info) => {
      stats[info.db_name] = info;
      return databases.getRequestDB().info();
    })
    .then((info) => {
      stats[info.db_name] = info;
      return databases.getFeedbackDB().info();
    })
    .then((info) => {
      stats[info.db_name] = info;
      return res.json(stats);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
});

/* Get hourly stats of applications and requests */
router.get("/hourlyStats", checkJwt, async (_req, res) => {
  let times = Array.apply(null, Array(24)).map((_x, i) => {
    return { time: `${i}`, requestcount: 0, applicationcount: 0 };
  });

  databases
    .getApplicationDB()
    .allDocs({ include_docs: true })
    .then((aDocs) => {
      aDocs.rows.forEach((row) => {
        const d = new Date(row.doc.datetime).toLocaleTimeString("FI", {
          timeZone: "Europe/Helsinki",
        });
        const hour = Number(d.substring(0, d.charAt(1) === "." ? 1 : 2));
        times[hour].applicationcount++;
      });

      return databases.getRequestDB().allDocs({ include_docs: true });
    })
    .then((rDocs) => {
      rDocs.rows.forEach((row) => {
        const d = new Date(row.doc.datetime).toLocaleTimeString("FI", {
          timeZone: "Europe/Helsinki",
        });
        const hour = Number(d.substring(0, d.charAt(1) === "." ? 1 : 2));
        times[hour].requestcount++;
      });

      return res.json(times);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
});

module.exports = router;

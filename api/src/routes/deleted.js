const express = require("express");
const databases = require("../databases");
const {
  getOneDeleted,
  getAllDeleted,
  restoreOne,
} = require("../controllers/databaseFunctions");

const { requiredScopes } = require("express-oauth2-jwt-bearer");
const checkAllDbScope = requiredScopes("all:database");

const router = express.Router();

const { checkJwt, validRequest } = require("../utils/helperFunctions");

/* Get one attachment of deleted application */
router.get(
  "/application/attachment/:id/:name",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) => {
    databases
      .getApplicationDB()
      .get(req.params.id, { revs: true, open_revs: "all" })
      .then((doc) => {
        let i = doc[0].ok;
        if (!i._deleted)
          return Promise.reject({
            status: 404,
            message: "Attachment not found.",
          });

        return databases
          .getApplicationDB()
          .getAttachment(req.params.id, req.params.name, {
            rev: i._revisions.start - 1 + "-" + i._revisions.ids[1],
          });
      })
      .then((buffer) => {
        return res.type(buffer.type).send(buffer);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

/* Get one deleted application request */
router.get(
  "/request/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) =>
    getOneDeleted(req.params.id, res, databases.getRequestDB())
);

/* Get one deleted feedback */
router.get(
  "/feedback/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) =>
    getOneDeleted(req.params.id, res, databases.getFeedbackDB())
);

/* Get one deleted application */
router.get(
  "/application/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) =>
    getOneDeleted(req.params.id, res, databases.getApplicationDB(), {
      attachments: true,
    })
);

/* Get all deleted application requests */
router.get(
  "/request",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (_req, res) => getAllDeleted(res, databases.getRequestDB())
);

/* Get all deleted application */
router.get(
  "/application",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (_req, res) =>
    getAllDeleted(res, databases.getApplicationDB(), { attachments: true })
);

/* Get all deleted feedbacks */
router.get(
  "/feedback",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (_req, res) => getAllDeleted(res, databases.getFeedbackDB())
);

/* Restore one deleted feedback */
router.post(
  "/feedback/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) => restoreOne(req.params.id, res, databases.getFeedbackDB())
);

/* Restore one deleted application request */
router.post(
  "/request/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) => restoreOne(req.params.id, res, databases.getRequestDB())
);

/* Restore one deleted application */
router.post(
  "/application/:id",
  checkJwt,
  checkAllDbScope,
  validRequest,
  async (req, res) =>
    restoreOne(req.params.id, res, databases.getApplicationDB(), {
      attachments: true,
    })
);

module.exports = router;

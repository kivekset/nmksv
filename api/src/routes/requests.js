const express = require("express");
const { body } = require("express-validator");
const databases = require("../databases");
const {
  getOne,
  getAll,
  deleteOne,
  deleteMany,
} = require("../controllers/databaseFunctions");

const { requiredScopes } = require("express-oauth2-jwt-bearer");
const checkReadScope = requiredScopes("read:applicationrequests");
const checkUpdateScope = requiredScopes("update:applicationrequests");
const checkDeleteScope = requiredScopes("delete:applicationrequests");

const {
  checkJwt,
  isApplyOpen,
  validRequest,
} = require("../utils/helperFunctions");
const { sendRequestEmail } = require("../services/emailService.js");

const router = express.Router();

/* Post new application request to database */
router.post(
  "/request",
  [
    body("email").isEmail({
      ignore_max_length: true,
      host_whitelist: ["tuni.fi"],
    }),
    body("email").isLength({ max: 100 }),
    body("name").not().isEmpty(),
    body("name").isLength({ max: 40 }),
    body("dataApproval").equals("true").toBoolean(),
  ],
  validRequest,
  async (req, res) => {
    // Check that the apply is open
    const isOpen = await isApplyOpen();
    if (!isOpen)
      return res.status(403).json({ error: "Hakeminen ei ole auki." });

    databases
      .getRequestDB()
      .find({
        selector: {
          email: { $eq: req.body.email },
        },
      })
      .then((result) => {
        // Check that the email does not already exist
        if (result.docs.length !== 0)
          return Promise.reject({
            status: 400,
            message: "Application request already asked for this email.",
          });

        let doc = {
          name: req.body.name,
          email: req.body.email,
          datetime: new Date().toISOString(),
          emailSent: false,
          dataApproval: req.body.dataApproval,
          applicationSent: false,
          applicationId: "",
          bypassWaitTime: false,
        };

        return databases.getRequestDB().post(doc);
      })
      .then((resDoc) => {
        return databases.getRequestDB().get(resDoc.id);
      })
      .then((sendDoc) => {
        return sendRequestEmail(sendDoc);
      })
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ error: err.message });
      });
  }
);

/* Bypass wait time for one application request */
router.post(
  "/applicationrequest/:id/bypasswait",
  checkJwt,
  checkUpdateScope,
  async (req, res) => {
    databases
      .getRequestDB()
      .get(req.params.id)
      .then((doc) => {
        let newDoc = {
          ...doc,
          bypassWaitTime: true,
        };

        return databases.getRequestDB().put(newDoc);
      })
      .then(() => {
        return res.sendStatus(200);
      })
      .catch((err) => {
        return res.status(err.status ?? 500).json({ errors: [err.message] });
      });
  }
);

/* Get single application requests authenticated */
router.get(
  "/applicationrequest/:id",
  checkJwt,
  checkReadScope,
  async (req, res) => getOne(req.params.id, res, databases.getRequestDB())
);

/* Get single application request public */
/* This is used with public application form as pre-data */
router.get("/request/:id", async (req, res) => {
  databases
    .getRequestDB()
    .get(req.params.id)
    .then((doc) => {
      // Check that application has not been yet sent
      if (doc.applicationSent)
        return Promise.reject({
          status: 403,
          message: "Hakemus on jo lähetetty.",
        });

      return res.json(doc);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
});

/* Get all application requests */
router.get(
  "/applicationrequests",
  checkJwt,
  checkReadScope,
  async (_req, res) => getAll(res, databases.getRequestDB())
);

/* Delete one application request */
router.delete(
  "/applicationrequest/:id",
  checkJwt,
  checkDeleteScope,
  async (req, res) => deleteOne(req.params.id, res, databases.getRequestDB())
);

/* Delete many application requests */
router.post(
  "/applicationrequest/delete",
  [body("ids").not().isEmpty()],
  checkJwt,
  checkDeleteScope,
  validRequest,
  async (req, res) => deleteMany(req.body.ids, res, databases.getRequestDB())
);

module.exports = router;

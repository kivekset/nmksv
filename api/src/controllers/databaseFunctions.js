/**
 * Get one document with given ID
 * @param {String} id ID of the document
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 * @param {Object} getOptions Extra options for get-function
 */
const getOne = async (id, res, database, getOptions) => {
  database
    .get(id, { ...getOptions })
    .then((doc) => {
      return res.json(doc);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Get one deleted document with given ID
 * @param {String} id ID of the deleted document
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 * @param {Object} getOptions Extra options for get-function
 */
const getOneDeleted = async (id, res, database, getOptions) => {
  // Get all revisions of the document
  database
    .get(id, { revs: true, open_revs: "all" })
    .then((doc) => {
      let i = doc[0].ok;
      if (!i._deleted)
        return Promise.reject({ status: 404, message: "Document not found." });

      // Get latest non-deleted version
      return database.get(id, {
        ...getOptions,
        rev: i._revisions.start - 1 + "-" + i._revisions.ids[1],
      });
    })
    .then((doc) => {
      return res.json(doc);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Get all documents
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 * @param {Object} allDocsOptions Extra options for allDocs-function
 */
const getAll = async (res, database, allDocsOptions) => {
  database
    .allDocs({ ...allDocsOptions, include_docs: true })
    .then((docs) => {
      return res.json(docs.rows.map((row) => row.doc));
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Get all deleted documents
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 * @param {Object} allDocsOptions Extra options for allDocs-function
 */
const getAllDeleted = async (res, database, getOptions) => {
  // Get all deleted feedbacks
  database
    .changes({ filter: (doc) => doc._deleted })
    .then((docs) => {
      // Get all revisions for all documents
      let getPromises = docs.results.map((item) => {
        return new Promise((resolve, reject) => {
          database
            .get(item.id, { revs: true, open_revs: "all" })
            .then((doc) => resolve(doc))
            .catch((err) => reject(err));
        });
      });

      return Promise.all(getPromises);
    })
    .then((docs) => {
      // Get latest non-deleted version of all documents
      let getPromises = docs.map((item) => {
        return new Promise((resolve, reject) => {
          let i = item[0].ok;
          database
            .get(i._id, {
              ...getOptions,
              rev: i._revisions.start - 1 + "-" + i._revisions.ids[1],
            })
            .then((doc) => resolve(doc))
            .catch((err) => reject(err));
        });
      });

      return Promise.all(getPromises);
    })
    .then((docs) => {
      return res.json(docs);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Delete one document
 * @param {String} id ID of the document to be deleted
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 */
const deleteOne = async (id, res, database) => {
  database
    .get(id)
    .then((doc) => {
      return database.remove(doc);
    })
    .then(() => {
      return res.sendStatus(200);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Delete multiple document
 * @param {String} ids IDs of the documents to be deleted
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 */
const deleteMany = async (ids, res, database) => {
  database
    .allDocs({ include_docs: true })
    .then((docs) => {
      return database.bulkDocs(
        docs.rows
          .filter((d) => ids.includes(d.id))
          .map((d) => {
            return { ...d.doc, _deleted: true };
          })
      );
    })
    .then(() => {
      return res.sendStatus(200);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

/**
 * Restore one deleted document with given ID
 * @param {String} id ID of the document
 * @param {Object} res Express response object
 * @param {Object} database PouchDB database
 * @param {Object} getOptions Extra options for get-function
 */
const restoreOne = async (id, res, database, getOptions) => {
  let deleteRev;
  // Get revision of the deleted version
  database
    .get(id, { revs: true, open_revs: "all" })
    .then((doc) => {
      let i = doc[0].ok;
      deleteRev = i._rev;
      if (!i._deleted)
        return Promise.reject({ status: 404, message: "Document not found." });

      // Get latest non-deleted version of the document
      return database.get(id, {
        ...getOptions,
        rev: i._revisions.start - 1 + "-" + i._revisions.ids[1],
      });
    })
    .then((doc) => {
      // Put the non-deleted document with deletion revision
      return database.put({ ...doc, _rev: deleteRev }, { force: true });
    })
    .then(() => {
      return res.sendStatus(200);
    })
    .catch((err) => {
      return res.status(err.status ?? 500).json({ errors: [err.message] });
    });
};

module.exports = {
  getOne,
  getOneDeleted,
  getAll,
  getAllDeleted,
  deleteOne,
  deleteMany,
  restoreOne,
};

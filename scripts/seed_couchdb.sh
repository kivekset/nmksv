#!/bin/bash -e
 
# This script is inspired from similar scripts in the Kitura BluePic project
 
# Find our current directory
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 
# Parse input parameters
url=http://localhost:5984
username=admin
password=dev
databases=( "application_db" "application_request_db" "news_db" )

# Create _users database
curl -X DELETE $url/_users -u $username:$password
curl -X PUT $url/_users -u $username:$password

# Create kives -user
curl -X PUT http://localhost:5984/_users/org.couchdb.user:kives \
	-u $username:$password \
	-H "Accept: application/json" \
	-H "Content-Type: application/json" \
	-d '{"name": "kives", "password": "kiveskives", "roles": [], "type": "user"}'

for database in "${databases[@]}"
do
	# Create database
	curl -X DELETE $url/$database -u $username:$password
	curl -X PUT $url/$database -u $username:$password

	# Add user privileges
	curl $url/$database/_security -X PUT \
		-u $username:$password \
		-H 'Content-Type: application/json' \
		-H 'Accept: application/json' \
		-d '{"admins":{"names":[],"roles":[]},"members":{"names": ["kives"],"roles": []}}'
done

# Run updates
/bin/bash $current_dir/update1.sh
/bin/bash $current_dir/update2.sh
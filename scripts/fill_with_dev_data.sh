url=http://localhost:5984
username=admin
password=dev

# Empty application_db
	curl -X DELETE $url/application_db -u $username:$password
	curl -X PUT $url/application_db -u $username:$password

# Add user privileges
curl $url/application_db/_security -X PUT \
  -u $username:$password \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{"admins":{"names":[],"roles":[]},"members":{"names": ["kives"],"roles": []}}'

# Empty application_request_db
	curl -X DELETE $url/application_request_db -u $username:$password
	curl -X PUT $url/application_request_db -u $username:$password

# Add user privileges
curl $url/application_request_db/_security -X PUT \
  -u $username:$password \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{"admins":{"names":[],"roles":[]},"members":{"names": ["kives"],"roles": []}}'

# First one, just basic request and application
curl $url/application_request_db/a4b910d31692994e7f4b12b7f8002b3b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "name": "ville",
        "email": "ville.tuominen@gmail.com",
        "datetime": "2021-12-05T10:32:49.810Z",
        "emailSent": true,
        "dataApproval": true,
        "applicationSent": true,
        "applicationId": "d995ed90a03cc98043c04ce73c00114b"
      }'

curl $url/application_db/d995ed90a03cc98043c04ce73c00114b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "requestId": "a4b910d31692994e7f4b12b7f8002b3b",
        "name": "ville",
        "email": "ville.tuominen@gmail.com",
        "datetime": "2021-12-06T09:39:42.404Z",
        "startYear": "2021",
        "studyField": "koulutus",
        "activityCV": "kokemus",
        "freeWord": "osaaminen",
        "reasonsToApply": "mukaan",
        "emailSent": true
      }'

# Second one, just basic request and application
curl $url/application_request_db/x4b910d31692994e7f4b12b7f8002b3b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "name": "kalle",
        "email": "kalle.saarinen@gmail.com",
        "datetime": "2021-11-05T11:32:49.810Z",
        "emailSent": true,
        "dataApproval": true,
        "applicationSent": true,
        "applicationId": "a995ed90a03cc98043c04ce73c00114b"
      }'

curl $url/application_db/a995ed90a03cc98043c04ce73c00114b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "requestId": "x4b910d31692994e7f4b12b7f8002b3b",
        "name": "kalle",
        "email": "kalle.saarinen@gmail.com",
        "datetime": "2021-12-01T16:39:42.404Z",
        "startYear": "2021",
        "studyField": "koulutus",
        "activityCV": "kokemus",
        "freeWord": "osaaminen",
        "reasonsToApply": "mukaan",
        "emailSent": true
      }'

# Third one, only request
curl $url/application_request_db/n4b910d31692994e7f4b12b7f8002b3b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "name": "antti",
        "email": "antti.anttinen@gmail.com",
        "datetime": "2021-11-07T13:32:49.810Z",
        "emailSent": true,
        "dataApproval": true,
        "applicationSent": false,
        "applicationId": ""
      }'

# Fourth one, only request
curl $url/application_request_db/n5b910d31692994e7f4b12b7f8002b3b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "name": "Kalle",
        "email": "kalle.kives@gmail.com",
        "datetime": "2021-11-07T11:32:49.810Z",
        "emailSent": true,
        "dataApproval": true,
        "applicationSent": false,
        "applicationId": ""
      }'

# Second one, emails not send
curl $url/application_request_db/f4b910d31692994e7f4b12b7f8002b3b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "name": "veli",
        "email": "veli@gmail.com",
        "datetime": "2021-11-22T23:32:49.810Z",
        "emailSent": false,
        "dataApproval": true,
        "applicationSent": true,
        "applicationId": "t995ed90a03cc98043c04ce73c00114b"
      }'

curl $url/application_db/t995ed90a03cc98043c04ce73c00114b -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{
        "requestId": "f4b910d31692994e7f4b12b7f8002b3b",
        "name": "veli",
        "email": "veli@gmail.com",
        "datetime": "2021-11-29T03:39:42.404Z",
        "startYear": "2021",
        "studyField": "koulutus",
        "activityCV": "kokemus",
        "freeWord": "osaaminen",
        "reasonsToApply": "mukaan",
        "emailSent": false
      }'

# Empty feedback_db
	curl -X DELETE $url/feedback_db -u $username:$password
	curl -X PUT $url/feedback_db -u $username:$password

# Add user privileges
curl $url/feedback_db/_security -X PUT \
  -u $username:$password \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{"admins":{"names":[],"roles":[]},"members":{"names": ["kives"],"roles": []}}'

# Add feedbacks
curl $url/feedback_db/05ca3ec0cee879801b9b99d8b0000c33 -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json;charset=utf-8' \
	-H 'Accept: application/json' \
	-d '{
        "name": "Ville",
        "text": "On tosi kivat sivut joo...",
        "email": "ville.mail",
        "telegram": "vikidi",
        "datetime": "2022-03-21T05:52:14.238Z"
      }'

curl $url/feedback_db/90fa3ec0cee879801b9b99d8b0000d42 -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json;charset=utf-8' \
	-H 'Accept: application/json' \
	-d '{
        "name": "",
        "text": "Palautetta\n\nuseammalla\nrivilla.\n\nYlla on ensin 2 enteria ja sitten 1 enter.",
        "email": "",
        "telegram": "",
        "datetime": "2022-04-07T12:51:58.240Z"
      }'

curl $url/feedback_db/23ba3ec0cee879801b9b99d8b0000zzt -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json;charset=utf-8' \
	-H 'Accept: application/json' \
	-d '{
        "name": "Kalle Kives",
        "text": "Testing testing",
        "email": "joku@gmail.com",
        "telegram": "@vikidi",
        "datetime": "2022-02-21T08:52:14.238Z"
      }'
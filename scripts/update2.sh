#!/bin/bash -e
 
# This script is inspired from similar scripts in the Kitura BluePic project
 
# Find our current directory
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
 
# Parse input parameters
url=http://localhost:5984
username=admin
password=dev

# Create database
curl -X DELETE $url/feedback_db -u $username:$password
curl -X PUT $url/feedback_db -u $username:$password

# Add user privileges
curl $url/feedback_db/_security -X PUT \
	-u $username:$password \
	-H 'Content-Type: application/json' \
	-H 'Accept: application/json' \
	-d '{"admins":{"names":[],"roles":[]},"members":{"names": ["kives"],"roles": []}}'
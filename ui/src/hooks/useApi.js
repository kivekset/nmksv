import { useEffect, useState } from 'react';
import { useAuth0 } from '@auth0/auth0-react';

import { decodeToken } from "react-jwt";

export const useDecodedToken = (propData = null) => {
  const { getAccessTokenSilently } = useAuth0();
  const [state, setState] = useState({
    decodedToken: propData
  });

  const [refreshIndex, setRefreshIndex] = useState(0);

  useEffect(() => {
    (async () => {
      // Don't fetch data if it is retrieved from props
      // Unless refreshed
      if (propData !== null && refreshIndex === 0) return;

      try {
        const accessToken = await getAccessTokenSilently({ audience: process.env.REACT_APP_AUTH_AUDIENCE });
        const dToken = decodeToken(accessToken);
        setState({ ...state, decodedToken: dToken });

      } catch (error) {
        setState({ ...state, decodedToken: null });
      }
    })();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getAccessTokenSilently, refreshIndex, propData])

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
}

export const useProtectedApi = (path, options = {method: "GET"}, propData = null) => {
  const { getAccessTokenSilently } = useAuth0();

  const [refreshIndex, setRefreshIndex] = useState(0);

  const [state, setState] = useState({
    error: null,
    status: null,
    errorMessage: null,
    loading: propData ? false : true,
    data: propData,
  });

  useEffect(() => {
    (async () => {
      // Don't fetch data if it is retrieved from props
      // Unless refreshed
      if (propData !== null && refreshIndex === 0) return;

      try {
        const { scope, ...fetchOptions } = options;
        const accessToken = await getAccessTokenSilently({ audience: process.env.REACT_APP_AUTH_AUDIENCE, scope: scope });
        
        let fullUrl;
        if (process.env.NODE_ENV === "production") {
          fullUrl = `https://${window.location.hostname.toString()}:5050${path}`
        } else if (process.env.NODE_ENV === "development") {
          fullUrl = `http://${window.location.hostname.toString()}:5050${path}`
        }

        const res = await fetch(fullUrl, {
          ...fetchOptions,
          headers: {
            ...fetchOptions.headers,
            // Expect always JSON
            "Content-Type": "application/json",
            // Add the Authorization header to the existing headers
            Authorization: `Bearer ${accessToken}`,
          },
        });

        const data = await res.json();

        if (!res?.ok) {
          return setState({
            data: null,
            error: true,
            errorMessage: data.error,
            status: res.status,
            loading: false,
          });
        }

        setState({
          data,
          error: null,
          errorMessage: null,
          status: res.status,
          loading: false,
        });

      } catch (error) {
        setState({
          data: null,
          error: true,
          errorMessage: error.message,
          status: null,
          loading: false,
        });
      }
    })();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getAccessTokenSilently, path, refreshIndex]); // Options kept refiring the hook

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
};

export const useApi = (path, options = {method: "GET"}, propData = null) => {
  const [refreshIndex, setRefreshIndex] = useState(0);

  const [state, setState] = useState({
    error: null,
    status: null,
    errorMessage: null,
    loading: propData ? false : true,
    data: propData,
  });

  useEffect(() => {
    (async () => {
      // Don't fetch data if it is retrieved from props
      // Unless refreshed
      if (propData !== null && refreshIndex === 0) return;

      try {
        const { ...fetchOptions } = options;
        
        let fullUrl;
        if (process.env.NODE_ENV === "production") {
          fullUrl = `https://${window.location.hostname.toString()}:5050${path}`
        } else if (process.env.NODE_ENV === "development") {
          fullUrl = `http://${window.location.hostname.toString()}:5050${path}`
        }

        const res = await fetch(fullUrl, {
          ...fetchOptions,
          headers: {
            ...fetchOptions.headers,
            // Expect always JSON
            "Content-Type": "application/json",
          },
        });

        const data = await res.json();

        if (!res?.ok) {
          return setState({
            data: null,
            error: true,
            status: res.status,
            errorMessage: data.error,
            loading: false,
          });
        }

        setState({
          data,
          error: null,
          errorMessage: null,
          status: res.status,
          loading: false,
        });

      } catch (error) {
        setState({
          data: null,
          error: true,
          status: null,
          errorMessage: error.message,
          loading: false,
        });
      }
    })();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [path, refreshIndex]); // Options kept refiring the hook

  return {
    ...state,
    refresh: () => setRefreshIndex(refreshIndex + 1),
  };
};
import { useState, useEffect } from "react";

export default function useCustomList(data, sortFields, searchFields) {
  const {
    filteredData,
    sortData,
    sortValue,
    setSortValue,
    ascSort,
    setAscSort,
  } = useSortList(data, sortFields);

  const { searchValue, setSearchValue } = useSearch(
    data,
    searchFields,
    sortData
  );

  const { selected, handleCheck, handleAllChecked } = useSelectList(
    filteredData
  );

  return {
    filteredData,
    sortValue,
    setSortValue,
    ascSort,
    setAscSort,
    selected,
    handleCheck,
    handleAllChecked,
    searchValue,
    setSearchValue,
  };
}

export const useSearch = (data, searchFields = [], sortData) => {
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    if (searchValue.trim() === "") {
      sortData(data);
      return;
    }

    const sVal = searchValue.toLowerCase();
    sortData(
      data.filter(
        (value) =>
          value &&
          searchFields
            .map(
              (field) =>
                value[field] && value[field].toLowerCase().includes(sVal)
            )
            .some((e) => e)
      )
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data, searchValue]);

  return {
    searchValue,
    setSearchValue,
  };
};

export const useSelectList = (data) => {
  const [selected, setSelected] = useState({ all: false, ids: [] });

  const handleCheck = (id) => {
    const currentIndex = selected.ids.indexOf(id);
    const newChecked = [...selected.ids];

    if (currentIndex === -1) {
      newChecked.push(id);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setSelected({
      ...selected,
      all: newChecked.length === data.length,
      ids: newChecked,
    });
  };

  const handleAllChecked = (e) => {
    const checked = e.target.checked;

    let newIDs;
    if (checked) {
      newIDs = data.map((d) => d._id);
    } else {
      newIDs = [];
    }

    setSelected({ ...selected, ids: newIDs, all: checked });
  };

  // Update selected values when filtered data changes
  // This way "hidden" data cannot be selected
  useEffect(() => {
    if (!data) return;

    const fDataIds = data.map((d) => d._id);
    const newIDs = selected.ids.filter((d) => fDataIds.includes(d));

    setSelected({
      ...selected,
      all: data.length !== 0 && newIDs.length === data.length,
      ids: newIDs,
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return {
    selected,
    handleCheck,
    handleAllChecked,
  };
};

const dateTimeCompare = (a, b, field) => {
  return a[field] > b[field] ? 1 : a[field] < b[field] ? -1 : 0;
};

const stringCompare = (a, b, field) => {
  return ("" + b[field].toLowerCase()).localeCompare(a[field].toLowerCase());
};

export const useSortList = (data, sortFields) => {
  const [filteredData, setFilteredData] = useState(null);
  const [sortValue, setSortValue] = useState(sortFields[0].value);
  const [ascSort, setAscSort] = useState(true);

  const sortData = (data) => {
    if (!data) return;

    const tempData = data.slice(0);

    const sortField = sortFields.filter((x) => x.value === sortValue)[0];
    const sortFunc =
      sortField.type === "string" ? stringCompare : dateTimeCompare;

    if (ascSort) {
      tempData.sort((b, a) => sortFunc(a, b, sortField.field));
    } else {
      tempData.sort((a, b) => sortFunc(a, b, sortField.field));
    }

    setFilteredData(tempData);
  };

  useEffect(() => {
    sortData(filteredData);

    // Don't fire when filteredData is self changed
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sortValue, ascSort]);

  return {
    filteredData,
    sortData,
    sortValue,
    setSortValue,
    ascSort,
    setAscSort,
  };
};

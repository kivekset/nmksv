let baseUrl;
if (process.env.NODE_ENV === "production") {
  baseUrl = `https://${window.location.hostname.toString()}:5050`;
} else if (process.env.NODE_ENV === "development") {
  baseUrl = `http://${window.location.hostname.toString()}:5050`;
}

export const callApi = (path, options = {}) => {
  return new Promise(async (resolve, reject) => {
    let url = `${baseUrl}${path}`

    try {
      const { header, ...fetchOptions } = options;

      const res = await fetch(url, {
        ...fetchOptions,
        headers: header
      });

      let data = res;
      if(fetchOptions.method === "GET" && !fetchOptions.responseType) {
        data = await res.json();
      }

      if (!data?.ok) {
        let body = await res.json();
        return reject({ status: data.status, body: body });
      }

      return resolve(data);

    } catch (error) {
      return reject(error);
    }
  });
}
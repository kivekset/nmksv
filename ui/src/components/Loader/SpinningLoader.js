import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from "@material-ui/core/styles"

import Icon from 'react-loadingg/lib/WaveLoading'

import styles from "assets/jss/material-kit-react/components/loaderStyle.js";
import { primaryColor } from "assets/jss/material-kit-react/components/loaderStyle.js";

const useStyles = makeStyles(styles);

const SpinningLoader = ({ size }) => {
  const classes = useStyles();

  return (
    <div className={classes.loader}>
      <Icon
        size={size}
        color={primaryColor}
      />
    </div>
  )
}

SpinningLoader.propTypes = {
  size: PropTypes.oneOf(['small', 'default', 'large']),
}

SpinningLoader.defaultProps = {
  size: 'small',
}

export default SpinningLoader
import React from "react";
import PropTypes from "prop-types";

import Button from "components/CustomButtons/Button.js";

// Modal window
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import Transition from "./Transition";

const PromptModal = props => {
  let { open, setOpen, onAccept, onClose, title, text } = props;
  onClose = onClose ?? (() => setOpen(false));

  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">
        {title}
      </DialogTitle>
      <DialogContent>
        {text.map((t, i) => {
          return (
            <DialogContentText id="alert-dialog-slide-description" key={i}>
              {t}
            </DialogContentText>
          )
        })}
      </DialogContent>
      <DialogActions>
        <Button
          color="primary"
          onClick={() => {
            setOpen(false);
            onAccept();
          }}
        >
          Kyllä
        </Button>
        <Button
          color="primary"
          onClick={onClose}
        >
          Eiku ":D"
        </Button>
      </DialogActions>
    </Dialog>
  )
};

PromptModal.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.array.isRequired,
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func,
  onAccept: PropTypes.func.isRequired,
  onClose: PropTypes.func
};

export default PromptModal;
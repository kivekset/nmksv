/*eslint-disable*/
import React, { useState, useEffect } from "react";

import { useAuth0 } from "@auth0/auth0-react";

import { Link } from 'react-router-dom';

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

import { useDecodedToken } from "hooks/useApi";

// @material-ui/icons
import { 
  AccessTimeOutlined,
  NoteAddOutlined,
  ReceiptOutlined,
  ExitToAppOutlined,
  FeedbackOutlined,
  StorageOutlined,
  GroupAddOutlined,
  RestoreFromTrashOutlined,
  TocOutlined } from "@material-ui/icons";

// core components
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();

  const { decodedToken } = useDecodedToken(props.decodedToken);
  const { logout } = useAuth0();

  const [anchorHaku, setAnchorHaku] = useState(null);
  const [anchorKanta, setAnchorKanta] = useState(null);

  useEffect(() => {
    const handler = () => {
      handleCloseHaku();
      handleCloseKanta();
    };
    window.addEventListener('scroll', handler);
    return () => {
      window.removeEventListener('scroll', handler);
    };
  }, []);

  const handleClickHaku = (event) => {
    setAnchorHaku(event.currentTarget);
  };

  const handleCloseHaku = () => {
    setAnchorHaku(null);
  };

  const handleClickKanta = (event) => {
    setAnchorKanta(event.currentTarget);
  };

  const handleCloseKanta = () => {
    setAnchorKanta(null);
  };

  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <div>
          <Button
            aria-controls="simple-menu" 
            aria-haspopup="true" 
            onClick={handleClickHaku}
            color="transparent"
            className={classes.navLink}
          >
            <GroupAddOutlined className={classes.icons} /> Haku
          </Button>
          <Menu
            id="haku-menu"
            anchorEl={anchorHaku}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            transformOrigin={{ vertical: "top", horizontal: "center" }}
            open={Boolean(anchorHaku)}
            onClose={handleCloseHaku}
            disableScrollLock
            autoFocus={false}
            className={classes.dropMenu}
          >
            <MenuItem
              component={Link}
              to="/admin/applytimes"
              className={classes.navLink}
            >
              <AccessTimeOutlined className={classes.icons} /> Hakuaika
            </MenuItem>
            {decodedToken?.permissions.includes("read:applicationrequests") && 
            <MenuItem
              component={Link}
              to="/admin/applicationrequests"
              className={classes.navLink}
            >
              <NoteAddOutlined className={classes.icons} /> Hakemuspyynnöt
            </MenuItem>}
            {decodedToken?.permissions.includes("read:applications") && 
            <MenuItem
              component={Link}
              to="/admin/applications"
              className={classes.navLink}
            >
              <ReceiptOutlined className={classes.icons} /> Hakemukset
            </MenuItem>}
          </Menu>
        </div>
      </ListItem>
      
      {decodedToken?.permissions.includes("read:feedback") && 
      <ListItem className={classes.listItem}>
        <Button
          href="/admin/feedbacks"
          color="transparent"
          className={classes.navLink}
        >
          <FeedbackOutlined className={classes.icons} /> Palautteet
        </Button>
      </ListItem>}
      {decodedToken?.permissions.includes("all:database") && 
      <ListItem className={classes.listItem}>
        <div>
          <Button
            aria-controls="simple-menu" 
            aria-haspopup="true" 
            onClick={handleClickKanta}
            color="transparent"
            className={classes.navLink}
          >
            <StorageOutlined className={classes.icons} /> Kanta
          </Button>
          <Menu
            id="kanta-menu"
            anchorEl={anchorKanta}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            transformOrigin={{ vertical: "top", horizontal: "center" }}
            open={Boolean(anchorKanta)}
            onClose={handleCloseKanta}
            disableScrollLock
            autoFocus={false}
            className={classes.dropMenu}
          >
            {decodedToken?.permissions.includes("all:database") && 
            <MenuItem
              component={Link}
              to="/admin/database"
              className={classes.navLink}
            >
              <TocOutlined className={classes.icons} /> Tietokannat
            </MenuItem>}
            {decodedToken?.permissions.includes("all:database") && 
            <MenuItem
              component={Link}
              to="/admin/restore"
              className={classes.navLink}
            >
              <RestoreFromTrashOutlined className={classes.icons} /> Palautus
            </MenuItem>}
          </Menu>
        </div>
      </ListItem>}
      <ListItem className={classes.listItem}>
        <Button
          onClick={() => logout({ returnTo: window.location.origin })}
          color="transparent"
          className={classes.navLink}
        >
          <ExitToAppOutlined className={classes.icons} /> Logout
        </Button>
      </ListItem>
    </List>
  );
}

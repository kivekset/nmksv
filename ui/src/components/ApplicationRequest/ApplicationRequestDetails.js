import React from "react";
import { Link } from "react-router-dom";
import Moment from "react-moment";

import GridItem from "components/Grid/GridItem.js";

export default function ApplicationRequestDetails({ data }) {
  return (
    <>
      <GridItem xs={12} sm={4} style={{ textAlign: "left" }}>
        <strong>Datakäyttö OK:</strong> {data.dataApproval.toString()}
      </GridItem>
      <GridItem xs={12} sm={4} style={{ textAlign: "left" }}>
        <strong>Säpo lähetetty:</strong> {data.emailSent.toString()}
      </GridItem>
      <GridItem xs={12} sm={4} style={{ textAlign: "left" }}>
        <strong>Hakemus lähetetty:</strong> {data.applicationSent.toString()}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Nimi:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.name}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Sähköposti:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.email}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Pyyntö jätetty:
      </GridItem>
      <GridItem xs={12} sm={6}>
        <Moment format="DD.MM.YYYY HH:mm">{data.datetime}</Moment>
        {data.bypassWaitTime ? " (odotusaika ohitettu)" : ""}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Hakemus:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {!data.applicationId || data.applicationId === "" ? (
          "Ei hakemusta"
        ) : (
          <Link to={`/admin/applications/${data.applicationId}`}>
            Hakemuslinkki
          </Link>
        )}
      </GridItem>
    </>
  );
}

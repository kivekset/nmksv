/*eslint-disable*/
import React, { useState, useEffect } from "react";

import { useAuth0 } from "@auth0/auth0-react";

import { useTranslation } from 'react-i18next';

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

// @material-ui/icons
import { GroupOutlined, ContactsOutlined, InfoOutlined, SecurityOutlined, LanguageOutlined } from "@material-ui/icons";

// Flags
import { GB, FI } from 'country-flag-icons/react/3x2';

// core components
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();

  const { isAuthenticated } = useAuth0();

  const { t, i18n } = useTranslation();

  const [anchorLanguage, setAnchorLanguage] = useState(null);

  useEffect(() => {
    const handler = () => {
      handleCloseLanguage();
    };
    window.addEventListener('scroll', handler);
    return () => {
      window.removeEventListener('scroll', handler);
    };
  }, []);

  const handleClickLanguage = (event) => {
    setAnchorLanguage(event.currentTarget);
  };

  const handleCloseLanguage = () => {
    setAnchorLanguage(null);
  };

  const changeLanguage = (language) => {
    i18n.changeLanguage(language);
    handleCloseLanguage();
  };

  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          href="/history"
          color="transparent"
          className={classes.navLink}
        >
          <GroupOutlined className={classes.icons} /> {t("Navigation.History")}
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/interested"
          color="transparent"
          className={classes.navLink}
        >
          <InfoOutlined className={classes.icons} /> {t("Navigation.Membership")}
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          href="/contact"
          color="transparent"
          className={classes.navLink}
        >
          <ContactsOutlined className={classes.icons} /> {t("Navigation.Contacts")}
        </Button>
      </ListItem>
      {isAuthenticated &&
      <ListItem className={classes.listItem}>
        <Button
          href="/admin"
          color="transparent"
          className={classes.navLink}
        >
          <SecurityOutlined className={classes.icons} /> Admin
        </Button>
      </ListItem>}
      <ListItem className={classes.listItem}>
        <div>
          <Button
            aria-controls="simple-menu" 
            aria-haspopup="true" 
            onClick={handleClickLanguage}
            color="transparent"
            className={classes.navLink}
          >
            <LanguageOutlined className={classes.icons} /> {t("Navigation.Language")}
          </Button>
          <Menu
            id="language-menu"
            anchorEl={anchorLanguage}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            transformOrigin={{ vertical: "top", horizontal: "center" }}
            open={Boolean(anchorLanguage)}
            onClose={handleCloseLanguage}
            disableScrollLock
            autoFocus={false}
            className={classes.dropMenu}
          >
            <MenuItem
              component={Button}
              onClick={() => changeLanguage("en")}
              className={classes.languageButton}
            >
              <GB title="English" /> English
            </MenuItem>
            <MenuItem
              component={Button}
              onClick={() => changeLanguage("fi")}
              className={classes.languageButton}
            >
              <FI title="Suomi" /> Suomi
            </MenuItem>
          </Menu>
        </div>
      </ListItem>
    </List>
  );
}

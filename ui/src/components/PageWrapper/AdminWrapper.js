import React from "react";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";

import AdminHeader from "components/AdminHeader/AdminHeader.js";
import AdminHeaderLinks from "components/AdminHeader/AdminHeaderLinks.js";
import PageWrapper from "./PageWrapper";

import styles from "assets/jss/material-kit-react/views/adminPanelView.js";
const useStyles = makeStyles(styles);

export default function AdminWrapper({ children, ...props }) {
  const classes = useStyles();

  return (
    <PageWrapper>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <AdminHeader
          brand="Admin Panel"
          refPage="/admin"
          rightLinks={<AdminHeaderLinks />}
          {...props}
        />
        <div className={classes.container}>{children}</div>
      </div>
    </PageWrapper>
  );
}

import React from "react";

import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";

export default function PageWrapper({ children, ...props }) {
  return (
    <div>
      <Header
        brand="NMKSV ry"
        refPage="/"
        rightLinks={<HeaderLinks />}
        fixed
        {...props}
      />
      {children}
      <Footer />
    </div>
  );
}

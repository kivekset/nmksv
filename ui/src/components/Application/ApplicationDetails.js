import React from "react";
import { Link } from "react-router-dom";
import Moment from "react-moment";

import GridItem from "components/Grid/GridItem.js";

export default function ApplicationDetails({ data, getAttachment }) {
  const attachmentButton = (key, type) => {
    return (
      <GridItem xs={12} md={4} key={key}>
        <>
          <GridItem style={{ padding: "0px", fontWeight: "bold" }}>
            <Link
              to="#"
              onClick={(e) => {
                // Prevent from reloading this page
                e.preventDefault();
                getAttachment(key);
              }}
            >
              {key}
            </Link>
          </GridItem>
          <GridItem style={{ padding: "0px" }}>
            {" "}
            Tyyppi: .{type.split("/")[1]}
          </GridItem>
        </>
      </GridItem>
    );
  };

  return (
    <>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Nimi:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.name}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Sähköposti:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.email}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Aloitusvuosi:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.startYear}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Opintosuunta:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.studyField}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Järjestö CV:
      </GridItem>
      <GridItem xs={12} sm={6} style={{ whiteSpace: "pre-line" }}>
        {data.activityCV}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Vapaa sana:
      </GridItem>
      <GridItem xs={12} sm={6} style={{ whiteSpace: "pre-line" }}>
        {data.freeWord}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Miksi hain:
      </GridItem>
      <GridItem xs={12} sm={6} style={{ whiteSpace: "pre-line" }}>
        {data.reasonsToApply}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Hakemus jätetty:
      </GridItem>
      <GridItem xs={12} sm={6}>
        <Moment format="DD.MM.YYYY HH:mm">{data.datetime}</Moment>
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Hakemuspyyntö:
      </GridItem>
      <GridItem xs={12} sm={6}>
        <Link to={`/admin/applicationrequests/${data.requestId}`}>
          Hakemuspyyntölinkki
        </Link>
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Säpo lähetetty:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.emailSent.toString()}
      </GridItem>
      <GridItem xs={12} style={{ fontWeight: "bold" }}>
        Liitteet:
      </GridItem>
      {data._attachments ? (
        Object.keys(data._attachments).map((key) =>
          attachmentButton(key, data._attachments[key].content_type)
        )
      ) : (
        <GridItem xs={12}>Ei liitteitä.</GridItem>
      )}
    </>
  );
}

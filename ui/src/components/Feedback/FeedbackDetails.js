import React from "react";

import Moment from "react-moment";

import GridItem from "components/Grid/GridItem.js";

export default function FeedbackDetails({ data }) {
  return (
    <>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Palaute:
      </GridItem>
      <GridItem xs={12} sm={6} style={{ whiteSpace: "pre-line" }}>
        {data.text}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Nimi:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.name}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Sähköposti:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.email}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Telegram:
      </GridItem>
      <GridItem xs={12} sm={6}>
        {data.telegram}
      </GridItem>
      <GridItem xs={12} sm={6} style={{ fontWeight: "bold" }}>
        Palaute jätetty:
      </GridItem>
      <GridItem xs={12} sm={6}>
        <Moment format="DD.MM.YYYY HH:mm">{data.datetime}</Moment>
      </GridItem>
    </>
  );
}

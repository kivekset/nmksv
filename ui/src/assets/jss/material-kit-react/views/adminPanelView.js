import { container } from "assets/jss/material-kit-react.js";

const adminPanelViewStyle = theme => ({
  container: {
    zIndex: "12",
    padding: "20px",
    ...container
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3"
  },
  mainRaised: {
    margin: "100px 40px 40px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
  },
  dataContainer: {
    padding: "0px 20px 40px 20px"
  },
  dataItem: {
    [theme.breakpoints.up('sm')]: {
      textAlign: 'center',
    }
  }
});

export default adminPanelViewStyle;
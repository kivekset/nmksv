import { container, title } from "assets/jss/material-kit-react.js";

import { primaryColor } from "assets/jss/material-kit-react.js";

import imagesStyle from "assets/jss/material-kit-react/imagesStyles.js";

const interestedPageStyle = {
  container: {
    zIndex: "12",
    color: "#FFFFFF",
    justify: "center",
    ...container
  },
  ingress: {
    fontWeight: 400
  },
  description: {
    margin: "1.071rem auto 0",
    maxWidth: "600px",
    color: "#666",
    textAlign: "center !important"
  },
  ...imagesStyle,
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3"
  },
  mainRaised: {
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
  },
  title: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none"
  },
  mainTitle: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  subTitle: {
    color: "#000"
  },
  sectionTitle: {
    color: "#000"
  },
  listText: {
    color: "#666"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  },
  focusedInput: {
    "&:hover:before,&:before": {
      borderColor: "#D2D2D2 !important",
      borderWidth: "1px !important"
    },
    "&:after": {
      borderColor: primaryColor
    }
  }
};

export default interestedPageStyle;

import React, { useState } from "react";
import { Link } from "react-router-dom";

import useMediaQuery from "@material-ui/core/useMediaQuery";

import { callApi } from "services/dataService";

import { useSnackbar } from "notistack";

//auth
import { useAuth0 } from "@auth0/auth0-react";
import { useDecodedToken, useProtectedApi } from "hooks/useApi";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";

import Moment from "react-moment";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationErrorsPage.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper.js";
import useCustomList from "hooks/useCustomList";
const useStyles = makeStyles(styles);

const ListItemLink = (props) => {
  return <ListItem button component="a" {...props} />;
};

const ApplicationsList = (props) => {
  const classes = useStyles();

  const { loading, data, refresh } = useProtectedApi("/applications", {
    scope: "read:applications",
    method: "GET",
  });

  const isXsUp = useMediaQuery((theme) => theme.breakpoints.up("sm"));

  const { enqueueSnackbar } = useSnackbar();

  const { decodedToken } = useDecodedToken();

  const [deleteState, setDeleteState] = useState({
    open: false,
    id: null,
  });

  const sortFields = [
    { value: "time", name: "Aika", field: "datetime", type: "datetime" },
    { value: "name", name: "Nimi", field: "name", type: "string" },
    { value: "email", name: "Säpo", field: "email", type: "string" },
  ];

  const {
    filteredData,
    sortValue,
    setSortValue,
    ascSort,
    setAscSort,
    selected,
    handleCheck,
    handleAllChecked,
    searchValue,
    setSearchValue,
  } = useCustomList(data, sortFields, ["name", "email"]);

  const [open, setOpen] = useState(false);

  const { getAccessTokenSilently } = useAuth0();

  const deleteApplication = (id) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:applications",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "DELETE",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/applications/application/${id}`, requestOptions);
      })
      .then(() => {
        refresh();
        enqueueSnackbar("Hakemus poistettu");
      })
      .catch(() => {
        setOpen(true);
      });
  };

  const [deleteManyState, setDeleteManyState] = useState({ open: false });

  const deleteMany = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:applications",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          body: JSON.stringify({
            ids: selected.ids,
          }),
        };

        return callApi(`/applications/application/delete`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar(
          `${selected.ids.length} ${
            selected.ids.length === 1 ? "hakemus" : "hakemusta"
          } poistettu`
        );
        refresh();
      })
      .catch(() => {
        setOpen(true);
      });
  };

  const applicantList = filteredData?.map((ad) => (
    <ListItem key={ad._id}>
      <Checkbox
        checked={selected.ids.includes(ad._id)}
        onChange={() => handleCheck(ad._id)}
        color="primary"
        tabIndex={-1}
        disableRipple
      />
      <ListItemLink
        component={Link}
        ContainerComponent={"div"}
        ContainerProps={{ style: { display: "contents" } }}
        to={{ pathname: "applications/" + ad._id, state: { application: ad } }}
      >
        <ListItemText
          primary={ad.name}
          secondary={ad.email}
          style={{ width: "60%" }}
        />
        {isXsUp && (
          <ListItemText
            secondary={<Moment format="DD.MM.YYYY HH:mm">{ad.datetime}</Moment>}
            style={{ width: "40%" }}
          />
        )}
        <ListItemSecondaryAction
          onClick={() => {
            if (decodedToken?.permissions.includes("delete:applications"))
              setDeleteState({ id: ad._id, open: true });
          }}
          style={{ marginRight: "10px" }}
        >
          <IconButton
            disabled={
              !decodedToken?.permissions.includes("delete:applications")
            }
            edge="end"
            aria-label="delete"
          >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemLink>
    </ListItem>
  ));

  return (
    <AdminWrapper>
      <GridContainer justifyContent="center">
        <GridItem xs={12} style={{ textAlign: "center", position: "absolute" }}>
          <h3 style={{ marginTop: "8px" }}>Hakemukset</h3>
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <div className={classes.root}>
            <IconButton
              color="inherit"
              aria-label="back"
              onClick={() => props.history.push("/admin")}
            >
              <ArrowBack />
            </IconButton>
            {loading && (
              <GridItem xs={12}>
                <SpinningLoader />
              </GridItem>
            )}
            {data && data.length > 0 ? (
              <>
                <div
                  style={
                    !isXsUp
                      ? {
                          display: "flex",
                          flexWrap: "wrap",
                          marginLeft: "16px",
                          marginRight: "16px",
                          marginTop: "10px",
                        }
                      : {
                          display: "flex",
                          marginLeft: "16px",
                          marginRight: "16px",
                          marginTop: "10px",
                        }
                  }
                >
                  <Checkbox
                    disabled={filteredData?.length === 0}
                    checked={selected?.all ?? false}
                    onChange={(e) => handleAllChecked(e)}
                    color="primary"
                    tabIndex={-1}
                    disableRipple
                    style={
                      !isXsUp
                        ? { order: 2, paddingRight: 0 }
                        : { order: 1, paddingRight: 0 }
                    }
                  />
                  <IconButton
                    disabled={
                      !decodedToken?.permissions.includes(
                        "delete:applications"
                      ) || selected.ids.length === 0
                    }
                    aria-label="delete-many"
                    onClick={() => setDeleteManyState({ open: true })}
                    style={
                      !isXsUp
                        ? { order: 3, padding: "8px" }
                        : { order: 2, padding: "8px", marginRight: "10px" }
                    }
                  >
                    {<DeleteIcon />}
                  </IconButton>
                  <TextField
                    value={searchValue}
                    onChange={(event) => {
                      setSearchValue(event.target.value);
                    }}
                    variant="outlined"
                    type="text"
                    placeholder="Haku"
                    fullWidth
                    size="small"
                    inputProps={{ maxLength: 20 }}
                    style={
                      !isXsUp
                        ? { order: 1, marginBottom: "20px" }
                        : { order: 3 }
                    }
                  />
                  <FormControl
                    size="small"
                    style={
                      !isXsUp
                        ? {
                            order: 4,
                            flexGrow: 1,
                            minWidth: "auto",
                            marginLeft: "5px",
                          }
                        : { order: 4, minWidth: "auto", marginLeft: "5px" }
                    }
                  >
                    <InputLabel id="sort-label" style={{ top: "-15px" }}>
                      Sort
                    </InputLabel>
                    <Select
                      labelId="sort-label"
                      value={sortValue}
                      onChange={(event) => setSortValue(event.target.value)}
                      variant="outlined"
                    >
                      {sortFields.map((f) => (
                        <MenuItem key={f.value} value={f.value}>
                          {f.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                  <IconButton
                    aria-label="sort-asc-desc"
                    onClick={() => setAscSort(!ascSort)}
                    style={{ order: 5, padding: "8px" }}
                  >
                    {ascSort ? <ArrowDownward /> : <ArrowUpward />}
                  </IconButton>
                </div>
                <GridItem style={{ paddingTop: "20px" }} xs={12}>
                  <Divider />
                </GridItem>
                <List component="nav" aria-label="main mailbox folders">
                  {applicantList}
                </List>
              </>
            ) : (
              !loading && (
                <GridItem xs={12} style={{ textAlign: "center" }}>
                  <h5>Ei hakemuksia.</h5>
                </GridItem>
              )
            )}
          </div>
        </GridItem>
      </GridContainer>

      <PromptModal
        open={deleteState.open}
        onClose={() => setDeleteState({ id: null, open: false })}
        onAccept={() => {
          deleteApplication(deleteState.id);
        }}
        setOpen={(param) => setDeleteState({ ...deleteState, open: param })}
        title="Varmistus"
        text={["Oletko varma, että haluat poistaa hakemuksen?"]}
      />

      <PromptModal
        open={deleteManyState.open}
        onClose={() => setDeleteManyState({ open: false })}
        onAccept={() => {
          deleteMany();
        }}
        setOpen={(param) => setDeleteManyState({ open: param })}
        title="Varmistus"
        text={[
          `Oletko varma, että haluat poistaa ${selected.ids.length} 
                    ${selected.ids.length === 1 ? "hakemuksen" : "hakemusta"}?`,
        ]}
      />

      <BasicModal
        open={open}
        setOpen={setOpen}
        title="Virhe!"
        text={["Poisto epäonnistui!"]}
      />
    </AdminWrapper>
  );
};

export default ApplicationsList;

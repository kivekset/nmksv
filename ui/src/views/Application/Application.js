import React, { useState, useEffect, useCallback } from "react";
import qs from "qs";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import classNames from "classnames";

import Button from "components/CustomButtons/Button.js";
import TextField from "@material-ui/core/TextField";

import { DropzoneArea } from "material-ui-dropzone";

import { useApi } from "../../hooks/useApi.js";
import { callApi } from "../../services/dataService";

import BasicModal from "components/Modal/BasicModal"
import PromptModal from "components/Modal/PromptModal"
import PageWrapper from "components/PageWrapper/PageWrapper.js";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationPage.js";

const HOUR_LIMIT = 12;

const FORM_DATA_KEY = "application_form_persistent_data";

const useStyles = makeStyles(styles);

const Application = props => {
  const { 
    loading: applyLoading, 
    error: applyError,
    data: applyData 
  } = useApi("/metadata/applyopenpublic");

  let id = qs.parse(props.location.search, { ignoreQueryPrefix: true }).id;

  const getSavedData = useCallback(() => {
    let data = localStorage.getItem(FORM_DATA_KEY);
    if (data) {
      // Parse it to a javaScript object
      try {
        data = JSON.parse(data);
        return data;
      } catch (err) {}
    }

    // Default data
    return {
      id,
      name: "",
      email: "",
      year: new Date().getFullYear(),
      study: "",
      activityCV: "",
      freeWord: "",
      reasons: "",
      files: []
    };
  }, [id]);

  const [state, setState] = useState(getSavedData());

  // Persist data when it changes
  useEffect(() => {
    localStorage.setItem(FORM_DATA_KEY, JSON.stringify(state));
  }, [state]);

  const { loading, error, data } = useApi(`/applications/request/${state.id}`);

  const classes = useStyles();

  // Modal states
  const [open, setOpen] = useState(false);
  const [openPrompt, setOpenPrompt] = useState(false);
  const [openFieldsMissing, setOpenFieldsMissing] = useState(false);
  const [openError, setOpenError] = useState(false);
  const [errorTexts, setErrorTexts] = useState([]);

  useEffect(() => {
    if (data) {
      setState({ ...state, name: data.name, email: data.email });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]); // Gives warning for state missing

  const waitTimePassed = datetime => {
    if (!datetime) return false;

    let requestDate = Date.parse(datetime);
    let diff = Math.abs(Date.now() - requestDate);
    let hours = Math.floor(diff / (1000 * 60 * 60));

    return (hours >= HOUR_LIMIT);
  }

  const handleSubmit = event => {
    event.preventDefault();

    if (
      state.name === "" ||
      state.year === "" ||
      state.study === ""
    ) {
      setOpenFieldsMissing(true);
      return;
    }

    setOpenPrompt(true);
  }

  const handleSend = () => {
    let formData = new FormData();
    
    if (state.files.length > 0) {
      for (let file of state.files) {
        formData.append("attachments", file);
      }
    }

    formData.append("requestId", state.id);
    formData.append("name", state.name);
    formData.append("email", state.email);
    formData.append("datetime", new Date().toISOString());
    formData.append("startYear", state.year);
    formData.append("studyField", state.study);
    formData.append("activityCV", state.activityCV);
    formData.append("freeWord", state.freeWord);
    formData.append("reasonsToApply", state.reasons);

    const requestOptions = {
      method: "POST",
      body: formData
    };
    
    callApi("/applications/application/", requestOptions)
      .then(() =>  {
        localStorage.removeItem(FORM_DATA_KEY);

        // Make sure prompt is closed since state might not have been updated yet
        setOpenPrompt(false);
        return setOpen(true);
      })
      .catch((err) => {
        setErrorTexts(err.body.errors.map(e => e.msg));

        // Make sure prompt is closed since state might not have been updated yet
        setOpenPrompt(false);
        return setOpenError(true);
      });
  };

  const applicationForm = () => {
    return (
      <>
        <form
          className="applicationForm"
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            required
            id="applicationName"
            label="Nimi"
            value={state.name}
            onChange={event => {
              setState({ ...state, name: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="text"
            placeholder="Nimi"
            inputProps={{ maxLength: 40 }}
            style={{ margin: 10, width: "80%" }}
          />

          <br></br>

          <TextField
            required
            id="applicationYear"
            label="Aloitusvuosi"
            value={state.year}
            onChange={event => {
              setState({ ...state, year: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="number"
            placeholder="Aloitusvuosi"
            inputProps={{ maxLength: 4 }}
            style={{ margin: 10, width: "80%" }}
          />

          <br></br>

          <TextField
            required
            id="applicationStudy"
            label="Koulutusala"
            value={state.study}
            onChange={event => {
              setState({ ...state, study: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="text"
            placeholder="Koulutusala"
            inputProps={{ maxLength: 40 }}
            style={{ margin: 10, width: "80%" }}
          />

          <br></br>

          <TextField
            id="applicationActivity"
            label="Aiempi kokemus opiskelijajärjestötoiminnasta"
            placeholder="<enter> -> rivinvaihto"
            value={state.activityCV}
            onChange={event => {
              setState({ ...state, activityCV: event.target.value });
            }}
            multiline
            variant="outlined"
            inputProps={{ maxLength: 500 }}
            style={{ margin: 10, width: "80%" }}
            helperText={`${state.activityCV.length}/500`}
            FormHelperTextProps={{ style: { textAlign: 'right' } }}
          />

          <br></br>

          <TextField
            id="applicationFree"
            label="Kerro itsestäsi ja osaamisestasi"
            placeholder="<enter> -> rivinvaihto"
            value={state.freeWord}
            onChange={event => {
              setState({ ...state, freeWord: event.target.value });
            }}
            multiline
            variant="outlined"
            inputProps={{ maxLength: 500 }}
            style={{ margin: 10, width: "80%" }}
            helperText={`${state.freeWord.length}/500`}
            FormHelperTextProps={{ style: { textAlign: 'right' } }}
          />

          <br></br>

          <TextField
            id="applicationReasons"
            label="Miksi haluaisit mukaan NMKSV:n toimintaan?"
            placeholder="<enter> -> rivinvaihto"
            value={state.reasons}
            onChange={event => {
              setState({ ...state, reasons: event.target.value });
            }}
            multiline
            variant="outlined"
            inputProps={{ maxLength: 500 }}
            style={{ margin: 10, width: "80%" }}
            helperText={`${state.reasons.length}/500`}
            FormHelperTextProps={{ style: { textAlign: 'right' } }}
          />

          <br></br>

          <center>
            <p>Esim. kuva ja/tai vapaamuotoinen hakemussetti (kuvatiedostot & pdf, max. 3 MB)</p>
            <div style={{ margin: 10, width: "80%" }}>
              <DropzoneArea
                filesLimit={3}
                maxFileSize={3000000}
                showFileNames={true}
                acceptedFiles={["image/*", "application/pdf"]}
                onChange={(files) => {
                  setState({ ...state, files: files });
                }}
              />
            </div>
          </center>

          <br></br>

          <Button
            type="submit"
            color="primary"
            variant="contained"
            id="btnApplication"
            style={{ margin: 10 }}
          >
            Lähetä hakemus
          </Button>
        </form>

        <BasicModal 
          open={openFieldsMissing}
          setOpen={setOpenFieldsMissing}
          title="Kenttiä puuttuu!"
          text={["Täytä vähintään pakolliset kohdat!"]}
        />

        <BasicModal 
          open={openError}
          setOpen={setOpenError}
          title="Virhe!"
          text={["Virhe hakemusta tallentaessa!", ...errorTexts]}
        />

        <PromptModal
          open={openPrompt}
          onAccept={() => handleSend()}
          setOpen={setOpenPrompt}
          title="Varmistus"
          text={[
            `Oletko varma, että haluat lähettää hakemuksesi? 
            Et voi muokata sitä enää tämän jälkeen.`
          ]}
        />

        <BasicModal 
          open={open}
          setOpen={setOpen}
          title="Hakemus tallennettu!"
          text={["Hakemus tallennettu! -> paita päälle -> lonkeroo niskaan!"]}
          onClose={() => {
            setOpen(false);
            props.history.push("/")
          }}
        />
      </>
    );
  }

  const loader = () => {
    return (
      <div style={{ marginTop: "100px" }}><SpinningLoader /></div>
    );
  }

  const applyErrorRender = () => {
    return (
      <h3>
        Virhe hakemuksen aukiolon kanssa! Ota meihin yhteyttä esim. 
        sähköpostilla kapteenisto(at)gmail.com
      </h3>
    );
  }

  const applyNotOpen = () => {
    return (
      <h3>
        Hakeminen ei ole tällä hetkellä auki. Ota meihin yhteyttä esim. 
        sähköpostilla kapteenisto(at)gmail.com jos tämä on mielestäsi virhe.
      </h3>
    );
  }

  const noApplicationFound = () => {
    return (
      <h3>
        Hakemuspyyntöäsi ei löydy tietokannasta tai se on jo käytetty. 
        Varmistathan, että osoite on sama kuin saamassasi sähköpostissa. 
        Jos tämäkään ei auta, olethan yhteydessä meidän hallitukseen esimerkiksi sähköpostilla
        kapteenisto(at)gmail.com.
      </h3>
    );
  }

  const timeNotFull = () => {
    return (
      <h3>
        Hakemuksesi ei ole vielä auki. Tähän kuluu 12 tuntia
        hakemustoiveesi jättämisestä. Palaa asiaan hieman myöhemmin.
      </h3>
    );
  }

  return (
    <PageWrapper>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <div className="Application" style={{ textAlign: "center" }}>
            <h1>NMKSV ry Hakemus</h1>

            {(applyLoading || loading) ? loader()
            : applyError ? applyErrorRender()
            : error ? noApplicationFound()
            : !applyData ? applyNotOpen()
            : !data ? noApplicationFound()
            : (!data.bypassWaitTime && !waitTimePassed(data.datetime)) ? timeNotFull()
            : applicationForm()}

          </div>
        </div>
      </div>
    </PageWrapper>
  );
};

Application.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object
};

export default Application;

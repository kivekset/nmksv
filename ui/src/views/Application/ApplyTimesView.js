import React, { useState } from "react";

// auth
import { useAuth0 } from "@auth0/auth0-react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { useDecodedToken, useProtectedApi } from "hooks/useApi.js";
import { callApi } from "services/dataService";

import useMediaQuery from "@material-ui/core/useMediaQuery";

import Moment from "react-moment";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Divider from "@material-ui/core/Divider";

import BasicModal from "components/Modal/BasicModal";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";

import { useSnackbar } from "notistack";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applyTimesView.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper.js";

const useStyles = makeStyles(styles);

const ApplyTimeView = (props) => {
  const classes = useStyles();

  const { loading, data, refresh } = useProtectedApi("/metadata/applyopen", {
    method: "GET",
  });

  const { decodedToken } = useDecodedToken();

  const [open, setOpen] = useState(false);

  const { getAccessTokenSilently } = useAuth0();

  const { enqueueSnackbar } = useSnackbar();

  const isXsUp = useMediaQuery((theme) => theme.breakpoints.up("sm"));

  const handleApplyOpen = (openValue) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "openid update:applyopen",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "PUT",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          body: JSON.stringify({ open: openValue }),
        };

        return callApi("/metadata/applyopen", requestOptions);
      })
      .then(() => {
        // Needs to be 'inverted' since still reflects state before changing
        enqueueSnackbar(`Haku ${data.isManuallyOpen ? "suljettu" : "avattu"}`);

        refresh();
      })
      .catch(() => {
        setOpen(true);
      });
  };

  const historyList = data?.updates
    ?.sort((a, b) => new Date(b.datetime) - new Date(a.datetime))
    .map((update) => (
      <ListItem key={update.datetime}>
        {!isXsUp && (
          <ListItemText
            primary={update.name}
            secondary={
              <Moment format="DD.MM.YYYY HH:mm">{update.datetime}</Moment>
            }
            style={{ textAlign: "center" }}
          />
        )}
        {isXsUp && (
          <>
            <ListItemText primary={update.name} secondary={update.email} />
            <ListItemText
              primary={
                <Moment format="DD.MM.YYYY HH:mm">{update.datetime}</Moment>
              }
              secondary={`${update.previousState ? "Auki" : "Kiinni"} -> ${
                update.newState ? "Auki" : "Kiinni"
              }`}
              style={{ textAlign: "end" }}
            />
          </>
        )}
      </ListItem>
    ));

  return (
    <AdminWrapper>
      <div style={{ textAlign: "-webkit-center" }}>
        <GridItem xs={12} md={8} style={{ textAlign: "center" }}>
          <IconButton
            style={{ left: 0, position: "absolute", paddingTop: "5px" }}
            color="inherit"
            aria-label="back"
            onClick={() => props.history.push("/admin")}
          >
            <ArrowBack />
          </IconButton>
          <h3>Hakuajat</h3>
        </GridItem>
        <div className={classes.root}>
          <GridContainer
            justifyContent="center"
            spacing={3}
            style={{ padding: "20px 0px 20px 0px" }}
          >
            {loading && (
              <GridItem xs={12}>
                <SpinningLoader />
              </GridItem>
            )}
            {data != null && (
              <>
                <br />
                <GridItem className={classes.dataItem} xs={12} sm={6}>
                  <strong>Haku auki:</strong> {data.isManuallyOpen.toString()}
                </GridItem>
                <GridItem className={classes.dataItem} xs={12} sm={6}>
                  <Button
                    disabled={
                      !decodedToken?.permissions.includes("update:applyopen")
                    }
                    color="primary"
                    onClick={() => handleApplyOpen(!data.isManuallyOpen)}
                  >
                    {data.isManuallyOpen ? "Sulje Haku" : "Avaa haku"}
                  </Button>
                </GridItem>
                <Divider />
                <GridItem style={{ paddingTop: "20px" }} xs={12}>
                  <Divider />
                </GridItem>
                <GridItem xs={12} style={{ textAlign: "center" }}>
                  <h4>Historia</h4>
                </GridItem>
                <GridItem xs={12} md={7}>
                  <List>{historyList}</List>
                </GridItem>
                {(!data.updates || data.updates.length === 0) && (
                  <GridItem xs={12} style={{ textAlign: "center" }}>
                    <h5>Ei historiaa.</h5>
                  </GridItem>
                )}
              </>
            )}
          </GridContainer>
        </div>

        <BasicModal
          open={open}
          setOpen={setOpen}
          title="Virhe!"
          text={["Haun päivittäminen epäonnistui!"]}
        />
      </div>
    </AdminWrapper>
  );
};

export default ApplyTimeView;

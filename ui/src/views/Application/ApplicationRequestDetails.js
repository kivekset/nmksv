import React, { useState } from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Divider from "@material-ui/core/Divider";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationErrorsPage.js";

import { useAuth0 } from "@auth0/auth0-react";
import { useDecodedToken, useProtectedApi } from "hooks/useApi";

import { callApi } from "services/dataService";

import { useSnackbar } from "notistack";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import SpinningLoader from "components/Loader/SpinningLoader.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper";
import ApplicationRequestDetails from "components/ApplicationRequest/ApplicationRequestDetails";

const useStyles = makeStyles(styles);

const ApplicationRequestDetail = (props) => {
  const classes = useStyles();
  const { id } = useParams();

  const propsAR = props.location.state ? props.location.state.request : null;

  const { loading, error, data, refresh } = useProtectedApi(
    `/applications/applicationrequest/${id}`,
    { method: "GET", scope: "read:applicationrequests" },
    propsAR
  );

  const { getAccessTokenSilently } = useAuth0();

  const { decodedToken } = useDecodedToken();

  const [openDelete, setOpenDelete] = useState(false);
  const [openDeleteError, setOpenDeleteError] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const deleteRequest = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:applicationrequests",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "DELETE",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(
          `/applications/applicationrequest/${id}`,
          requestOptions
        );
      })
      .then(() => {
        enqueueSnackbar("Hakemuspyyntö poistettu");
        props.history.replace("/admin/applicationrequests");
      })
      .catch(() => {
        setOpenDeleteError(true);
      });
  };

  const bypassWaitTime = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "update:applicationrequests",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(
          `/applications/applicationrequest/${id}/bypasswait`,
          requestOptions
        );
      })
      .then(() => {
        enqueueSnackbar("Hakemuspyynnön odotusaika ohitettu");
        refresh();
      })
      .catch(() => {
        enqueueSnackbar("Hakemuspyynnön aikaohitus epäonnistui!", {
          variant: "error",
        });
      });
  };

  return (
    <AdminWrapper>
      <GridContainer justifyContent="center">
        <GridItem xs={12} style={{ textAlign: "center", position: "absolute" }}>
          <h3 style={{ marginTop: "8px" }}>Hakemuspyyntö</h3>
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <div className={classes.root}>
            <IconButton
              color="inherit"
              aria-label="back"
              onClick={() => props.history.push("/admin/applicationrequests")}
            >
              <ArrowBack />
            </IconButton>
            {error ? (
              <p>Etsimääsi hakemusta ei löydy</p>
            ) : loading ? (
              <div style={{ paddingTop: "100px" }}>
                <SpinningLoader />
              </div>
            ) : (
              <GridContainer
                spacing={3}
                style={{ padding: "20px 0px 20px 0px" }}
              >
                <ApplicationRequestDetails data={data} />

                <GridItem xs={12}>
                  <Divider />
                </GridItem>

                <GridItem xs={12} sm={4}>
                  <Button
                    color="primary"
                    onClick={() => setOpenDelete(true)}
                    disabled={
                      !decodedToken?.permissions.includes(
                        "delete:applicationrequests"
                      )
                    }
                  >
                    Poista pyyntö
                  </Button>
                </GridItem>

                <GridItem xs={12} sm={4}>
                  <Button
                    color="primary"
                    onClick={() => bypassWaitTime()}
                    disabled={
                      data.bypassWaitTime ||
                      !decodedToken?.permissions.includes(
                        "update:applicationrequests"
                      )
                    }
                  >
                    Ohita odotusaika
                  </Button>
                </GridItem>
              </GridContainer>
            )}
          </div>
        </GridItem>
      </GridContainer>

      <PromptModal
        open={openDelete}
        onAccept={() => {
          deleteRequest();
        }}
        setOpen={setOpenDelete}
        title="Varmistus"
        text={["Oletko varma, että haluat poistaa hakemuspyynnön?"]}
      />

      <BasicModal
        open={openDeleteError}
        setOpen={setOpenDeleteError}
        title="Virhe!"
        text={["Hakemuspyynnön poisto epäonnistui!"]}
      />
    </AdminWrapper>
  );
};

ApplicationRequestDetail.propTypes = {
  location: PropTypes.object,
};

export default ApplicationRequestDetail;

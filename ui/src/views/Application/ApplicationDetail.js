import React, { useState } from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Divider from "@material-ui/core/Divider";

import { useAuth0 } from "@auth0/auth0-react";

import { callApi } from "services/dataService";
import { useDecodedToken, useProtectedApi } from "hooks/useApi";

import { useSnackbar } from "notistack";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationErrorsPage.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper";
import ApplicationDetails from "components/Application/ApplicationDetails";

const useStyles = makeStyles(styles);

const ApplicationDetail = (props) => {
  const classes = useStyles();
  const { id } = useParams();

  const { getAccessTokenSilently } = useAuth0();

  const { decodedToken } = useDecodedToken();

  let propsApplication = props.location.state
    ? props.location.state.application
    : null;

  const { loading, error, data } = useProtectedApi(
    `/applications/application/${id}`,
    { method: "GET", scope: "read:applications" },
    propsApplication
  );

  const [open, setOpen] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openDeleteError, setOpenDeleteError] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const deleteApplication = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:applications",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "DELETE",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/applications/application/${id}`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar("Hakemus poistettu");
        props.history.replace("/admin/applications");
      })
      .catch(() => {
        setOpenDeleteError(true);
      });
  };

  const getAttachment = (key) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "read:applications",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "GET",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          responseType: "blob",
        };

        return callApi(
          `/applications/application/attachment/${id}/${key}`,
          requestOptions
        );
      })
      .then(async (response) => {
        response.blob().then((blob) => {
          //Build a URL from the blob
          const fileURL = URL.createObjectURL(blob);
          window.open(fileURL);
        });
      })
      .catch(() => {
        setOpen(true);
      });
  };

  return (
    <AdminWrapper>
      <GridContainer justifyContent="center">
        <GridItem xs={12} style={{ textAlign: "center", position: "absolute" }}>
          <h3 style={{ marginTop: "8px" }}>Hakemus</h3>
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <div className={classes.root}>
            <IconButton
              color="inherit"
              aria-label="back"
              onClick={() => props.history.push("/admin/applications")}
            >
              <ArrowBack />
            </IconButton>
            {error ? (
              <p>Etsimääsi hakemusta ei löydy</p>
            ) : loading ? (
              <div style={{ paddingTop: "100px" }}>
                <SpinningLoader />
              </div>
            ) : (
              <GridContainer
                spacing={3}
                style={{ padding: "20px 0px 20px 0px" }}
              >
                <ApplicationDetails data={data} getAttachment={getAttachment} />

                <GridItem xs={12}>
                  <Divider />
                </GridItem>

                <GridItem xs={12} sm={4}>
                  <Button
                    color="primary"
                    onClick={() => setOpenDelete(true)}
                    disabled={
                      !decodedToken?.permissions.includes("delete:applications")
                    }
                  >
                    Poista hakemus
                  </Button>
                </GridItem>
              </GridContainer>
            )}
          </div>
        </GridItem>
      </GridContainer>

      <PromptModal
        open={openDelete}
        onAccept={() => {
          deleteApplication();
        }}
        setOpen={setOpenDelete}
        title="Varmistus"
        text={["Oletko varma, että haluat poistaa hakemuksen?"]}
      />

      <BasicModal
        open={open}
        setOpen={setOpen}
        title="Virhe!"
        text={["Liitteen haku epäonnistui!"]}
      />

      <BasicModal
        open={openDeleteError}
        setOpen={setOpenDeleteError}
        title="Virhe!"
        text={["Hakemuksen poisto epäonnistui!"]}
      />
    </AdminWrapper>
  );
};

ApplicationDetail.propTypes = {
  location: PropTypes.object,
};

export default ApplicationDetail;

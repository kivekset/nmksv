import React, { useState } from "react";
import { Link } from "react-router-dom";

import useMediaQuery from "@material-ui/core/useMediaQuery";

import { callApi } from "services/dataService";

import { useSnackbar } from "notistack";

//auth
import { useAuth0 } from "@auth0/auth0-react";
import { useDecodedToken, useProtectedApi } from "hooks/useApi";
import useCustomList from "hooks/useCustomList";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";

import Moment from "react-moment";

import Checkbox from "@material-ui/core/Checkbox";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import ArrowBack from "@material-ui/icons/ArrowBack";
import TextField from "@material-ui/core/TextField";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationErrorsPage.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper.js";
const useStyles = makeStyles(styles);

const ListItemLink = (props) => {
  return <ListItem button {...props} />;
};

const FeedbackList = (props) => {
  const classes = useStyles();

  const { loading, data, refresh } = useProtectedApi("/feedback", {
    method: "GET",
    scope: "read:feedback",
  });

  const { enqueueSnackbar } = useSnackbar();

  const { decodedToken } = useDecodedToken();

  const [deleteState, setDeleteState] = useState({
    open: false,
    id: null,
  });

  const sortFields = [
    { value: "time", name: "Aika", field: "datetime", type: "datetime" },
  ];

  const {
    filteredData,
    ascSort,
    setAscSort,
    selected,
    handleCheck,
    handleAllChecked,
    searchValue,
    setSearchValue,
  } = useCustomList(data, sortFields, ["name", "email", "telegram"]);

  const [open, setOpen] = useState(false);

  const { getAccessTokenSilently } = useAuth0();

  const isXsUp = useMediaQuery((theme) => theme.breakpoints.up("sm"));

  const deleteFeedback = (id) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:feedback",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "DELETE",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/feedback/${id}`, requestOptions);
      })
      .then(() => {
        refresh();
        enqueueSnackbar("Palaute poistettu");
      })
      .catch(() => {
        setOpen(true);
      });
  };

  const [deleteManyState, setDeleteManyState] = useState({ open: false });

  const deleteMany = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "delete:feedback",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          body: JSON.stringify({
            ids: selected.ids,
          }),
        };

        return callApi(`/feedback/delete`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar(
          `${selected.ids.length} ${
            selected.ids.length === 1 ? "palaute" : "palautetta"
          } poistettu`
        );
        refresh();
      })
      .catch(() => {
        setOpen(true);
      });
  };

  const feedbackList = filteredData?.map((ad) => (
    <ListItem key={ad._id}>
      <Checkbox
        checked={selected.ids.includes(ad._id)}
        onChange={() => handleCheck(ad._id)}
        color="primary"
        tabIndex={-1}
        disableRipple
      />
      <ListItemLink
        component={Link}
        ContainerComponent={"div"}
        ContainerProps={{ style: { display: "contents" } }}
        to={{ pathname: "feedbacks/" + ad._id, state: { feedback: ad } }}
      >
        <ListItemText
          primary={`${ad.text.substring(0, 30)}${
            ad.text.length > 30 ? "..." : ""
          }`}
          secondary={<Moment format="DD.MM.YYYY HH:mm">{ad.datetime}</Moment>}
          style={{ width: "60%" }}
        />
        {isXsUp && (
          <ListItemText
            primary={ad.email?.trim() === "" ? "-" : ad.email}
            secondary={ad.telegram?.trim() === "" ? "-" : ad.telegram}
            style={{ width: "40%" }}
          />
        )}
        <ListItemSecondaryAction
          onClick={() => {
            if (decodedToken?.permissions.includes("delete:feedback"))
              setDeleteState({ id: ad._id, open: true });
          }}
          style={{ marginRight: "10px" }}
        >
          <IconButton
            disabled={!decodedToken?.permissions.includes("delete:feedback")}
            edge="end"
            aria-label="delete"
          >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemLink>
    </ListItem>
  ));

  return (
    <AdminWrapper>
      <GridContainer justifyContent="center">
        <GridItem xs={12} style={{ textAlign: "center", position: "absolute" }}>
          <h3 style={{ marginTop: "8px" }}>Palauteet</h3>
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <div className={classes.root}>
            <IconButton
              color="inherit"
              aria-label="back"
              onClick={() => props.history.push("/admin")}
            >
              <ArrowBack />
            </IconButton>
            {loading && (
              <GridItem xs={12}>
                <SpinningLoader />
              </GridItem>
            )}
            {data && data.length > 0 ? (
              <>
                <div
                  style={{
                    display: "flex",
                    marginLeft: "16px",
                    marginRight: "16px",
                    marginTop: "10px",
                  }}
                >
                  <Checkbox
                    disabled={filteredData?.length === 0}
                    checked={selected?.all ?? false}
                    onChange={(e) => handleAllChecked(e)}
                    color="primary"
                    tabIndex={-1}
                    disableRipple
                    style={{ paddingRight: 0 }}
                  />
                  <IconButton
                    disabled={
                      !decodedToken?.permissions.includes("delete:feedback") ||
                      selected.ids.length === 0
                    }
                    aria-label="delete-many"
                    onClick={() => setDeleteManyState({ open: true })}
                    style={{ padding: "8px", marginRight: "10px" }}
                  >
                    {<DeleteIcon />}
                  </IconButton>
                  <TextField
                    value={searchValue}
                    onChange={(event) => {
                      setSearchValue(event.target.value);
                    }}
                    variant="outlined"
                    type="text"
                    placeholder="Haku"
                    fullWidth
                    size="small"
                    inputProps={{ maxLength: 20 }}
                  />
                  <IconButton
                    aria-label="sort-asc-desc"
                    onClick={() => setAscSort(!ascSort)}
                    style={{ padding: "8px" }}
                  >
                    {ascSort ? <ArrowDownward /> : <ArrowUpward />}
                  </IconButton>
                </div>
                <GridItem style={{ paddingTop: "20px" }} xs={12}>
                  <Divider />
                </GridItem>
                <List component="nav" aria-label="main mailbox folders">
                  {feedbackList}
                </List>
              </>
            ) : (
              !loading && (
                <GridItem xs={12} style={{ textAlign: "center" }}>
                  <h5>Ei palautteita.</h5>
                </GridItem>
              )
            )}
          </div>
        </GridItem>
      </GridContainer>

      <PromptModal
        open={deleteState.open}
        onClose={() => setDeleteState({ id: null, open: false })}
        onAccept={() => {
          deleteFeedback(deleteState.id);
        }}
        setOpen={(param) => setDeleteState({ ...deleteState, open: param })}
        title="Varmistus"
        text={["Oletko varma, että haluat poistaa palautteen?"]}
      />

      <PromptModal
        open={deleteManyState.open}
        onClose={() => setDeleteManyState({ open: false })}
        onAccept={() => {
          deleteMany();
        }}
        setOpen={(param) => setDeleteManyState({ open: param })}
        title="Varmistus"
        text={[
          `Oletko varma, että haluat poistaa ${selected.ids.length} 
                    ${
                      selected.ids.length === 1 ? "palautteen" : "palautetta"
                    }?`,
        ]}
      />

      <BasicModal
        open={open}
        setOpen={setOpen}
        title="Virhe!"
        text={["Poisto epäonnistui!"]}
      />
    </AdminWrapper>
  );
};

export default FeedbackList;

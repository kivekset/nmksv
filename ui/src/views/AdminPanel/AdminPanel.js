import React, { useState, useEffect } from "react";

import useMediaQuery from '@material-ui/core/useMediaQuery';

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import { LineChart, Line, CartesianGrid, XAxis, YAxis, Legend, Tooltip } from "recharts";

import { useProtectedApi } from "../../hooks/useApi.js";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/adminPanelView.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper.js";
const useStyles = makeStyles(styles);

const AdminPanel = () => {
  const classes = useStyles();

  const { loading, data, error } = useProtectedApi("/metadata/adminstats");

  const { 
    loading: loadingHourly,
    error: errorHourly,
    data: dataHourly
  } = useProtectedApi("/metadata/hourlyStats");

  const isComputer = useMediaQuery("(min-width:600px)");

  const [width, setWidth] = useState(window.innerWidth);

  useEffect(() => {
    const updateWindowDimensions = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", updateWindowDimensions);

    return () => window.removeEventListener("resize", updateWindowDimensions)
  }, []);

  return (
    <AdminWrapper>
      <GridContainer spacing={3} className={classes.dataContainer}>
        <GridItem xs={12} style={{ textAlign: "center" }}>
          <h2>Admin Statistics</h2>
        </GridItem>
        {loading &&
        <GridItem xs={12}><SpinningLoader /></GridItem>
        }
        {error &&
        <GridItem xs={12}>Datan haku epäonnistui!</GridItem>
        }
        {data &&
        <>
          <GridItem xs={12} sm={6} md={3} className={classes.dataItem}>
            <strong>Auki:</strong> {data.open.toString()}
          </GridItem>
          <GridItem xs={12} sm={6} md={3} className={classes.dataItem}>
            <strong>Hakemuspyyntöjä:</strong> {data.requestCount.toString()}
          </GridItem>
          <GridItem xs={12} sm={6} md={3} className={classes.dataItem}>
            <strong>Hakemuksia:</strong> {data.applicationCount.toString()}
          </GridItem>
          <GridItem xs={12} sm={6} md={3} className={classes.dataItem}>
            <strong>Palautteita:</strong> {data.feedbackCount.toString()}
          </GridItem>
        </>
        }
        {(loadingHourly && isComputer) && <GridItem xs={12}><SpinningLoader /></GridItem>}
        {(errorHourly && isComputer) && 
          <GridItem xs={12} style={{ textAlign: "center" }}>Datan haku epäonnistui!</GridItem>
        }
        {(dataHourly && isComputer) && 
          <>
            <GridItem xs={12}>
              <div style={{ textAlign: "center" }}>
                <h3>Pyyntöjen ja hakemusten jättöajat</h3>
              </div>
            </GridItem>
            <GridItem xs={12}>
              <div style={{textAlign: "-webkit-center"}}>
                  <LineChart 
                    data={dataHourly.sort((a, b) => a.time - b.time)}
                    width={width < 900 ? width * 0.7 : width * 0.6} 
                    height={width * 0.2}
                  >
                    <CartesianGrid vertical={false} strokeDasharray="4 2" />
                    <XAxis height={40} label={{ value: 'Klo', position: 'insideBottomRight', offset: 0 }} dataKey="time" />
                    <YAxis allowDecimals={false} width={20} />
                    <Tooltip labelFormatter={value => `Klo: ${value}`} />
                    <Legend />
                    <Line name="Hakemukset" type="monotone" dataKey="applicationcount" dot={false} stroke="#8884d8" />
                    <Line name="Hakemuspyynnöt" type="monotone" dataKey="requestcount" dot={false} stroke="#82ca9d" />
                  </LineChart>
              </div>
            </GridItem>
          </>
        }
      </GridContainer>
    </AdminWrapper>
  );
};

export default AdminPanel;

import React, { useState } from "react";

// auth
import { useAuth0 } from "@auth0/auth0-react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { callApi } from "services/dataService";

import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Divider from "@material-ui/core/Divider";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";

import { useDecodedToken, useProtectedApi } from "hooks/useApi";

import { useSnackbar } from "notistack";

import { humanFileSize } from "utils/helpers.js";

//import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/adminPanelView.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper";

const useStyles = makeStyles(styles);

const ApplyTimeView = (props) => {
  const classes = useStyles();

  const { data, refresh } = useProtectedApi("/metadata/dbstats");

  const [purgeState, setPurgeState] = useState({
    open: false,
    name: null,
  });

  const { decodedToken } = useDecodedToken();

  const [open, setOpen] = useState(false);

  const { getAccessTokenSilently } = useAuth0();

  const { enqueueSnackbar } = useSnackbar();

  const handleDBPurge = (db) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/dbadmin/purge/${db}`, requestOptions);
      })
      .then(() => {
        refresh();
        enqueueSnackbar(`DB ${db} tyhjennetty vanhoista.`);
      })
      .catch(() => {
        setOpen(true);
      });
  };

  return (
    <AdminWrapper>
      <div style={{ textAlign: "-webkit-center" }}>
        <GridItem xs={12} md={8} style={{ textAlign: "center" }}>
          <IconButton
            style={{ left: 0, position: "absolute", paddingTop: "5px" }}
            color="inherit"
            aria-label="back"
            onClick={() => props.history.push("/admin")}
          >
            <ArrowBack />
          </IconButton>
          <h3>Tietokannat</h3>
        </GridItem>
        <div className={classes.root}>
          <GridContainer
            justifyContent="center"
            spacing={3}
            style={{ padding: "20px 0px 20px 0px" }}
          >
            <>
              <br />
              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <strong>
                  <h4>Applications</h4>
                </strong>
              </GridItem>
              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <Button
                  disabled={!decodedToken?.permissions.includes("all:database")}
                  color="primary"
                  onClick={() =>
                    setPurgeState({ open: true, name: "applications" })
                  }
                >
                  Tyhjennä poistetut
                </Button>
              </GridItem>

              {data && (
                <>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Dokumentteja:</strong>{" "}
                    {data["application_db"].doc_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Poistettuja:</strong>{" "}
                    {data["application_db"].doc_del_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Koko:</strong>{" "}
                    {humanFileSize(data["application_db"].sizes.active)}
                  </GridItem>
                </>
              )}

              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <strong>
                  <h4>Application Requests</h4>
                </strong>
              </GridItem>
              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <Button
                  disabled={!decodedToken?.permissions.includes("all:database")}
                  color="primary"
                  onClick={() =>
                    setPurgeState({ open: true, name: "applicationrequests" })
                  }
                >
                  Tyhjennä poistetut
                </Button>
              </GridItem>

              {data && (
                <>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Dokumentteja:</strong>{" "}
                    {data["application_request_db"].doc_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Poistettuja:</strong>{" "}
                    {data["application_request_db"].doc_del_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Koko:</strong>{" "}
                    {humanFileSize(data["application_request_db"].sizes.active)}
                  </GridItem>
                </>
              )}

              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <strong>
                  <h4>Feedback</h4>
                </strong>
              </GridItem>
              <GridItem className={classes.dataItem} xs={12} sm={6}>
                <Button
                  disabled={!decodedToken?.permissions.includes("all:database")}
                  color="primary"
                  onClick={() =>
                    setPurgeState({ open: true, name: "feedbacks" })
                  }
                >
                  Tyhjennä poistetut
                </Button>
              </GridItem>

              {data && (
                <>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Dokumentteja:</strong>{" "}
                    {data["feedback_db"].doc_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Poistettuja:</strong>{" "}
                    {data["feedback_db"].doc_del_count}
                  </GridItem>
                  <GridItem xs={12} md={3} className={classes.dataItem}>
                    <strong>Koko:</strong>{" "}
                    {humanFileSize(data["feedback_db"].sizes.active)}
                  </GridItem>
                </>
              )}

              <GridItem xs={12}>
                <Divider />
              </GridItem>
            </>
          </GridContainer>
        </div>

        <PromptModal
          open={purgeState.open}
          onClose={() => setPurgeState({ name: null, open: false })}
          onAccept={() => {
            handleDBPurge(purgeState.name);
          }}
          setOpen={(param) => setPurgeState({ ...purgeState, open: param })}
          title="Varmistus"
          text={[
            `Oletko varma, että haluat lopullisesti tyhjentää poistetut dokumentit kannasta ${purgeState.name}?`,
          ]}
        />

        <BasicModal
          open={open}
          setOpen={setOpen}
          title="Virhe!"
          text={[" Vanhojen tyhjennys epäonnistui!"]}
        />
      </div>
    </AdminWrapper>
  );
};

export default ApplyTimeView;

import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from 'react-i18next';

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/views/landingPage.js";

// Sections for this page
import InfoSection from "./Sections/InfoSection";
import EventSection from "./Sections/EventSection.js";
import PageWrapper from "components/PageWrapper/PageWrapper";

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();

  const { t } = useTranslation();

  return (
    <PageWrapper
      color="transparent"
      changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
    >
      <Parallax
        filter
        image={
          window.innerWidth <= 600
            ? require("assets/img/alicante_600.jpg")
            : require("assets/img/alicante_1280.jpg")
        }
      >
        <div className={classes.container}>
        <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>{t("LandingPage.Title")}</h1>
              <h4 className={classes.ingress}>{t("LandingPage.Description")}</h4>
              {window.innerHeight > 600 && <br />}
              <Button
                color="primary"
                size="lg"
                href="/interested"
                rel="noopener noreferrer"
              >
                {t("LandingPage.MembershipButton")}
              </Button>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <InfoSection t={t} />
          <EventSection t={t} />
        </div>
      </div>
    </PageWrapper>
  );
}

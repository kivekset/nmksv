import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import NavPills from "components/NavPills/NavPills.js";

import styles from "assets/jss/material-kit-react/views/history.js";

const useStyles = makeStyles(styles);

export default function EventSection({ t }) {
  const classes = useStyles();
  return (
    <div className={classes.section} style={{ paddingTop: "0px" }}>
      <GridContainer justifyContent="center">
        <GridItem cs={12} sm={12} md={8}>
          <h2 className={classes.sectionTitle}>
            {t("LandingPage.EventSection.Title")}
          </h2>
          <h4 className={classes.description}>
            {t("LandingPage.EventSection.Description")}
          </h4>
        </GridItem>
      </GridContainer>
      <GridContainer justifyContent="center">
        <GridItem cs={12} sm={12} md={8}>
          <NavPills
            color="orange"
            horizontal={{
              tabsGrid: { xs: 12, sm: 4, md: 4 },
              contentGrid: { xs: 12, sm: 8, md: 8 },
            }}
            tabs={[
              {
                tabButton: t("LandingPage.EventSection.Wapunkaato.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.Wapunkaato.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Wapunkaato.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Wapunkaato.Section2")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Wapunkaato.Section3")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.Kiveskuppi.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.Kiveskuppi.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Kiveskuppi.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Kiveskuppi.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.RJK.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.RJK.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.RJK.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.RJK.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.WTF2F.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.WTF2F.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.WTF2F.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.WTF2F.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.Charity.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.Charity.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Charity.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Charity.Section2")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Charity.Section3")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.PinkitSitsit.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.PinkitSitsit.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.PinkitSitsit.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.PinkitSitsit.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.SpedenSpelit.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.SpedenSpelit.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.SpedenSpelit.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.SpedenSpelit.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.Quidditch.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.Quidditch.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Quidditch.Section1")}
                    </p>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.Quidditch.Section2")}
                    </p>
                  </span>
                ),
              },
              {
                tabButton: t("LandingPage.EventSection.PinkkiXQ.Title"),
                tabContent: (
                  <span>
                    <h4 className={classes.eventTitle}>
                      {t("LandingPage.EventSection.PinkkiXQ.Subtitle")}
                    </h4>
                    <p className={classes.paragraph}>
                      {t("LandingPage.EventSection.PinkkiXQ.Section1")}
                    </p>
                  </span>
                ),
              },
            ]}
          />
        </GridItem>
      </GridContainer>
    </div>
  );
}

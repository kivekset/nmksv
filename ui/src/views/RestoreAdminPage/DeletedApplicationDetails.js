import React, { useState } from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";
import Divider from "@material-ui/core/Divider";

import { useAuth0 } from "@auth0/auth0-react";

import { callApi } from "services/dataService";
import { useDecodedToken, useProtectedApi } from "hooks/useApi";

import { useSnackbar } from "notistack";

import BasicModal from "components/Modal/BasicModal";
import PromptModal from "components/Modal/PromptModal";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/applicationErrorsPage.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper";
import ApplicationDetails from "components/Application/ApplicationDetails";

const useStyles = makeStyles(styles);

const ApplicationDetail = (props) => {
  const classes = useStyles();
  const { id } = useParams();

  const { getAccessTokenSilently } = useAuth0();

  const { decodedToken } = useDecodedToken();

  let propsApplication = props.location.state
    ? props.location.state.application
    : null;

  const { loading, error, data } = useProtectedApi(
    `/deleted/application/${id}`,
    { method: "GET", scope: "read:applications" },
    propsApplication
  );

  const [open, setOpen] = useState(false);
  const [openRestore, setOpenRestore] = useState(false);
  const [openRestoreError, setOpenRestoreError] = useState(false);

  const { enqueueSnackbar } = useSnackbar();

  const RestoreApplication = () => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/deleted/application/${id}`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar("Hakemus palautettu");
        props.history.replace("/admin/restore");
      })
      .catch(() => {
        setOpenRestoreError(true);
      });
  };

  const getAttachment = (key) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "GET",
          header: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${accessToken}`,
          },
          responseType: "blob",
        };

        return callApi(
          `/deleted/application/attachment/${id}/${key}`,
          requestOptions
        );
      })
      .then(async (response) => {
        response.blob().then((blob) => {
          //Build a URL from the blob
          const fileURL = URL.createObjectURL(blob);
          window.open(fileURL);
        });
      })
      .catch(() => {
        setOpen(true);
      });
  };

  return (
    <AdminWrapper>
      <GridContainer justifyContent="center">
        <GridItem xs={12} style={{ textAlign: "center", position: "absolute" }}>
          <h3 style={{ marginTop: "8px" }}>Poistettu Hakemus</h3>
        </GridItem>
        <GridItem xs={12} sm={12} md={8}>
          <div className={classes.root}>
            <IconButton
              color="inherit"
              aria-label="back"
              onClick={() => props.history.push("/admin/restore")}
            >
              <ArrowBack />
            </IconButton>
            {error ? (
              <p>Etsimääsi hakemusta ei löydy</p>
            ) : loading ? (
              <div style={{ paddingTop: "100px" }}>
                <SpinningLoader />
              </div>
            ) : (
              <GridContainer
                spacing={3}
                style={{ padding: "20px 0px 20px 0px" }}
              >
                <ApplicationDetails data={data} getAttachment={getAttachment} />

                <GridItem xs={12}>
                  <Divider />
                </GridItem>

                <GridItem xs={12} sm={4}>
                  <Button
                    color="primary"
                    onClick={() => setOpenRestore(true)}
                    disabled={
                      !decodedToken?.permissions.includes("all:database")
                    }
                  >
                    Palauta Hakemus
                  </Button>
                </GridItem>
              </GridContainer>
            )}
          </div>
        </GridItem>
      </GridContainer>

      <PromptModal
        open={openRestore}
        onAccept={() => {
          RestoreApplication();
        }}
        setOpen={setOpenRestore}
        title="Varmistus"
        text={["Oletko varma, että haluat palauttaa hakemuksen?"]}
      />

      <BasicModal
        open={open}
        setOpen={setOpen}
        title="Virhe!"
        text={["Liitteen haku epäonnistui!"]}
      />

      <BasicModal
        open={openRestoreError}
        setOpen={setOpenRestoreError}
        title="Virhe!"
        text={["Hakemuksen palautus epäonnistui!"]}
      />
    </AdminWrapper>
  );
};

ApplicationDetail.propTypes = {
  location: PropTypes.object,
};

export default ApplicationDetail;

import React from "react";
import { Link } from "react-router-dom";

import useMediaQuery from "@material-ui/core/useMediaQuery";

// auth
import { useAuth0 } from "@auth0/auth0-react";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import Moment from "react-moment";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Divider from "@material-ui/core/Divider";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import RestoreFromTrashOutlined from "@material-ui/icons/RestoreFromTrashOutlined";

import IconButton from "@material-ui/core/IconButton";
import ArrowBack from "@material-ui/icons/ArrowBack";

import { useDecodedToken, useProtectedApi } from "hooks/useApi";
import { callApi } from "services/dataService";

import { useSnackbar } from "notistack";

import SpinningLoader from "components/Loader/SpinningLoader.js";

// Just some style that contains basic css
import styles from "assets/jss/material-kit-react/views/restoreAdminPageView.js";
import AdminWrapper from "components/PageWrapper/AdminWrapper";

const ListItemLink = (props) => {
  return <ListItem button {...props} />;
};

const useStyles = makeStyles(styles);

const RestoreAdminPage = (props) => {
  const classes = useStyles();

  const {
    loading: feedbackLoading,
    data: feedbackData,
    refresh: refreshFeedback,
  } = useProtectedApi("/deleted/feedback", {
    method: "GET",
    scope: "all:database",
  });

  const {
    loading: requestLoading,
    data: requestData,
    refresh: refreshRequest,
  } = useProtectedApi("/deleted/request", {
    method: "GET",
    scope: "all:database",
  });

  const {
    loading: applicationLoading,
    data: applicationData,
    refresh: refreshApplication,
  } = useProtectedApi("/deleted/application", {
    method: "GET",
    scope: "all:database",
  });

  const { decodedToken } = useDecodedToken();

  const isXsUp = useMediaQuery((theme) => theme.breakpoints.up("sm"));

  const { getAccessTokenSilently } = useAuth0();

  const { enqueueSnackbar } = useSnackbar();

  const restoreFeedback = (id) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/deleted/feedback/${id}`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar("Palaute palautettu poistetuista");
        refreshFeedback();
      })
      .catch(() => {
        enqueueSnackbar("Palautteen palauttaminen epäonnistui!", {
          variant: `error`,
        });
      });
  };

  const restoreRequest = (id, rev) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/deleted/request/${id}`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar("Hakemuspyyntö palautettu poistetuista");
        refreshRequest();
      })
      .catch(() => {
        enqueueSnackbar("Hakemuspyynnön palauttaminen epäonnistui!", {
          variant: `error`,
        });
      });
  };

  const restoreApplication = (id, rev) => {
    getAccessTokenSilently({
      audience: process.env.REACT_APP_AUTH_AUDIENCE,
      scope: "all:database",
    })
      .then((accessToken) => {
        const requestOptions = {
          method: "POST",
          header: {
            Authorization: `Bearer ${accessToken}`,
          },
        };

        return callApi(`/deleted/application/${id}`, requestOptions);
      })
      .then(() => {
        enqueueSnackbar("Hakemus palautettu poistetuista");
        refreshApplication();
      })
      .catch(() => {
        enqueueSnackbar("Hakemuksen palauttaminen epäonnistui!", {
          variant: `error`,
        });
      });
  };

  const feedbackList = feedbackData?.map((ad) => (
    <ListItem key={ad._id}>
      <ListItemLink
        component={Link}
        ContainerComponent={"div"}
        ContainerProps={{ style: { display: "contents" } }}
        to={{
          pathname: "/admin/deleted/feedback/" + ad._id,
          state: { feedback: ad },
        }}
      >
        <ListItemText
          primary={`${ad.text.substring(0, 30)}${
            ad.text.length > 30 ? "..." : ""
          }`}
          secondary={<Moment format="DD.MM.YYYY HH:mm">{ad.datetime}</Moment>}
          style={{ width: "60%" }}
        />
        {isXsUp && (
          <ListItemText
            primary={ad.email?.trim() === "" ? "-" : ad.email}
            secondary={ad.telegram?.trim() === "" ? "-" : ad.telegram}
            style={{ width: "40%" }}
          />
        )}
        <ListItemSecondaryAction
          style={{ marginRight: "10px" }}
          onClick={() => {
            if (decodedToken?.permissions.includes("all:database"))
              restoreFeedback(ad._id);
          }}
        >
          <IconButton
            disabled={!decodedToken?.permissions.includes("all:database")}
            edge="end"
            aria-label="delete"
          >
            <RestoreFromTrashOutlined />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemLink>
    </ListItem>
  ));

  const requestList = requestData?.map((ad) => (
    <ListItem key={ad._id}>
      <ListItemLink
        component={Link}
        ContainerComponent={"div"}
        ContainerProps={{ style: { display: "contents" } }}
        to={{
          pathname: "/admin/deleted/request/" + ad._id,
          state: { request: ad },
        }}
      >
        <ListItemText
          primary={ad.name}
          secondary={ad.email}
          style={{ width: "60%" }}
        />
        {isXsUp && (
          <ListItemText
            secondary={<Moment format="DD.MM.YYYY HH:mm">{ad.datetime}</Moment>}
            style={{ width: "40%" }}
          />
        )}
        <ListItemSecondaryAction
          style={{ marginRight: "10px" }}
          onClick={() => {
            if (decodedToken?.permissions.includes("all:database"))
              restoreRequest(ad._id);
          }}
        >
          <IconButton
            disabled={!decodedToken?.permissions.includes("all:database")}
            edge="end"
            aria-label="delete"
          >
            <RestoreFromTrashOutlined />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemLink>
    </ListItem>
  ));

  const applicationList = applicationData?.map((ad) => (
    <ListItem key={ad._id}>
      <ListItemLink
        component={Link}
        ContainerComponent={"div"}
        ContainerProps={{ style: { display: "contents" } }}
        to={{
          pathname: "/admin/deleted/application/" + ad._id,
          state: { application: ad },
        }}
      >
        <ListItemText
          primary={ad.name}
          secondary={ad.email}
          style={{ width: "60%" }}
        />
        {isXsUp && (
          <ListItemText
            secondary={<Moment format="DD.MM.YYYY HH:mm">{ad.datetime}</Moment>}
            style={{ width: "40%" }}
          />
        )}
        <ListItemSecondaryAction
          style={{ marginRight: "10px" }}
          onClick={() => {
            if (decodedToken?.permissions.includes("all:database"))
              restoreApplication(ad._id);
          }}
        >
          <IconButton
            disabled={!decodedToken?.permissions.includes("all:database")}
            edge="end"
            aria-label="delete"
          >
            <RestoreFromTrashOutlined />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemLink>
    </ListItem>
  ));

  return (
    <AdminWrapper>
      <div style={{ textAlign: "-webkit-center" }}>
        <GridItem xs={12} md={8} style={{ textAlign: "center" }}>
          <IconButton
            style={{ left: 0, position: "absolute", paddingTop: "5px" }}
            color="inherit"
            aria-label="back"
            onClick={() => props.history.push("/admin")}
          >
            <ArrowBack />
          </IconButton>
          <h3>Palautus</h3>
        </GridItem>
        <div className={classes.root}>
          <GridContainer
            justifyContent="center"
            style={{ padding: "20px 0px 20px 0px" }}
          >
            <>
              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12}>
                <strong>
                  <h4>Poistetut Hakemuspyynnöt</h4>
                </strong>
              </GridItem>
              <div style={{ width: "80%" }}>
                {requestLoading && (
                  <GridItem xs={12}>
                    <SpinningLoader />
                  </GridItem>
                )}
                {requestData && requestData.length > 0 ? (
                  <>
                    <List component="nav" aria-label="main mailbox folders">
                      {requestList}
                    </List>
                  </>
                ) : (
                  !requestLoading && (
                    <GridItem xs={12} style={{ textAlign: "center" }}>
                      <h5>Ei poistettuja hakemuspyyntöjä.</h5>
                    </GridItem>
                  )
                )}
              </div>

              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12}>
                <strong>
                  <h4>Poistetut Hakemukset</h4>
                </strong>
              </GridItem>
              <div style={{ width: "80%" }}>
                {applicationLoading && (
                  <GridItem xs={12}>
                    <SpinningLoader />
                  </GridItem>
                )}
                {applicationData && applicationData.length > 0 ? (
                  <>
                    <List component="nav" aria-label="main mailbox folders">
                      {applicationList}
                    </List>
                  </>
                ) : (
                  !applicationLoading && (
                    <GridItem xs={12} style={{ textAlign: "center" }}>
                      <h5>Ei poistettuja hakemuksia.</h5>
                    </GridItem>
                  )
                )}
              </div>

              <GridItem xs={12}>
                <Divider />
              </GridItem>

              <GridItem className={classes.dataItem} xs={12}>
                <strong>
                  <h4>Poistetut Palautteet</h4>
                </strong>
              </GridItem>
              <div style={{ width: "80%" }}>
                {feedbackLoading && (
                  <GridItem xs={12}>
                    <SpinningLoader />
                  </GridItem>
                )}
                {feedbackData && feedbackData.length > 0 ? (
                  <>
                    <List component="nav" aria-label="main mailbox folders">
                      {feedbackList}
                    </List>
                  </>
                ) : (
                  !feedbackLoading && (
                    <GridItem xs={12} style={{ textAlign: "center" }}>
                      <h5>Ei poistettuja palautteita.</h5>
                    </GridItem>
                  )
                )}
              </div>

              <GridItem xs={12}>
                <Divider />
              </GridItem>
            </>
          </GridContainer>
        </div>
      </div>
    </AdminWrapper>
  );
};

export default RestoreAdminPage;

import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import Button from "components/CustomButtons/Button.js";
import TextField from "@material-ui/core/TextField";

import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import FormHelperText from "@material-ui/core/FormHelperText";
import { callApi } from "../../../services/dataService";

import BasicModal from "components/Modal/BasicModal"

import pdf from "../../../assets/documents/tietosuojaseloste.pdf";
import pdf_en from "../../../assets/documents/tietosuojaseloste_en.pdf";

import styles from "assets/jss/material-kit-react/views/interestedPage.js";

import { useApi } from "../../../hooks/useApi.js";

import SpinningLoader from "components/Loader/SpinningLoader.js";

const useStyles = makeStyles(styles);

export default function InfoSection({t, i18n}) {
  const classes = useStyles();

  const [open, setOpen] = useState(false);
  const [error, setError] = useState(false);

  // Accept terms checked
  const [checked, setChecked] = useState(false);
  const [acceptError, setAcceptError] = useState(false);

  // Name and email
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");

  // Email error
  const [emailError, setEmailError] = useState("");
  const [hasEmailError, setEmailErrorBool] = useState(false);

  // Name error
  const [nameError, setNameError] = useState("");
  const [hasNameError, setNameErrorBool] = useState(false);

  const { loading, error: dataError, data } = useApi("/metadata/applyopenpublic");

  const changeEmail = event => {
    setEmail(event.target.value);
    setEmailError("");
    setEmailErrorBool(false);
  };

  const changeName = event => {
    setName(event.target.value);
    setNameError("");
    setNameErrorBool(false);
  };

  const handleCheck = e => {
    setAcceptError(false);
    setChecked(e.target.checked);
  };

  const validateEmail = () => {
    return email.trim() !== "" && email.endsWith("@tuni.fi");
  };

  const handleSubmit = event => {
    event.preventDefault();

    if (name.trim() === "") {
      setNameError(t("InterestedPage.ApplicationSection.NameEmptyError"));
      setNameErrorBool(true);
      return;
    }

    if (process.env.NODE_ENV === "production") {
      if (!validateEmail()) {
        setEmailError(t("InterestedPage.ApplicationSection.NotTuniError"));
        setEmailErrorBool(true);
        return;
      }
    }

    if (!checked) {
      setAcceptError(true);
      return;
    }

    const requestOptions = {
      method: "POST",
      header: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        name: name,
        dataApproval: checked
      })
    }

    callApi("/applications/request", requestOptions)
      .then(() => {
        setError(false);
        setOpen(true);
        setEmail("");
        setName("");
        setChecked(false);
      })
      .catch(() => {
        setError(true);
        setOpen(true);
      });
  };

  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <GridContainer justifyContent="center">
          {loading && 
          <GridItem xs={12}>
            <SpinningLoader />
          </GridItem>}
          {data &&
          <GridItem xs={12} sm={4} md={8}>
            <div>
              <h4 className={classes.subTitle}>
                {t("InterestedPage.ApplicationSection.Title")}
              </h4>
            </div>
            <form
              className="interestedForm"
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit}
            >
              <GridContainer justifyContent="center">
                <GridItem xs={12} sm={12} md={6}>
                  <TextField
                    className={classes.customInput}
                    required
                    fullWidth
                    helperText={nameError}
                    error={hasNameError}
                    id="interestedName"
                    label={t("InterestedPage.ApplicationSection.NamePlaceholder")}
                    value={name}
                    onChange={changeName}
                    type="text"
                    inputProps={{ maxLength: 40 }}
                    InputProps={{
                      classes: {
                        focused: classes.focusedInput
                      }
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <TextField
                    className={classes.customInput}
                    required
                    fullWidth
                    helperText={emailError}
                    error={hasEmailError}
                    id="interestedEmail"
                    label={t("InterestedPage.ApplicationSection.EmailPlaceholder")}
                    value={email}
                    onChange={changeEmail}
                    type="email"
                    inputProps={{ maxLength: 100 }}
                    InputProps={{
                      classes: {
                        focused: classes.focusedInput
                      }
                    }}
                  />
                </GridItem>
              </GridContainer>

              <FormControlLabel
                control={
                  <Checkbox
                    checked={checked}
                    onChange={handleCheck}
                    color="primary"
                    name="allowance"
                  />
                }
                label={t("InterestedPage.ApplicationSection.AcceptanceText")}
              />
              {acceptError && (
                <FormHelperText style={{ color: "red" }}>
                  {t("InterestedPage.ApplicationSection.AccptanceError")}
                </FormHelperText>
              )}

              <GridContainer justifyContent="center">
                <Button
                  type="submit"
                  color="primary"
                  size="lg"
                  style={{ marginTop: 20, marginBottom: 20 }}
                >
                  {t("InterestedPage.ApplicationSection.ApplyButton")}
                </Button>
              </GridContainer>

              <BasicModal 
                open={open}
                setOpen={setOpen}
                title={error ? t("InterestedPage.ApplicationSection.SendError") : t("InterestedPage.ApplicationSection.RequestSaved")}
                text={
                  error
                    ? [t("InterestedPage.ApplicationSection.SendErrorText")]
                    : [t("InterestedPage.ApplicationSection.RequestSendText1"),
                       t("InterestedPage.ApplicationSection.RequestSendText2")]
                }
              />
            </form>
          </GridItem>
          }
          {((!loading && !data) || dataError) && 
          <GridItem xs={12} style={{marginBottom: "20px"}}>
            <h5 className={classes.description}>
              <strong>{t("InterestedPage.ApplicationSection.ApplyNotOpen.Part1")}</strong>{t("InterestedPage.ApplicationSection.ApplyNotOpen.Part2")}
            </h5>
          </GridItem>
          }
          <GridItem xs={12} style={{marginBottom: "50px", textAlign: "center"}}>
            <a href={i18n.language === "fi" ? pdf : pdf_en} target="_blank" rel="noopener noreferrer">
              {t("InterestedPage.ApplicationSection.PrivacyPolicy")}
            </a>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}

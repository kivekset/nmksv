import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import styles from "assets/jss/material-kit-react/views/interestedPage.js";

const useStyles = makeStyles(styles);

export default function InfoSection({t}) {
    const classes = useStyles();

  return (
    <div className={classes.section}>
      <GridContainer justifyContent="center">
        <GridItem xs={12} sm={12} md={8}>
            <h3 className={classes.sectionTitle}>{t("InterestedPage.CriteriaSection.Title")}</h3>
            <h4 className={classes.subTitle}>{t("InterestedPage.CriteriaSection.Subtitle1")}</h4>
            <List>
                <ListItem>
                    <ListItemText className={classes.listText} primary={t("InterestedPage.CriteriaSection.MainCriteria.Criteria1")} />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText} primary={t("InterestedPage.CriteriaSection.MainCriteria.Criteria2")} />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText} primary={t("InterestedPage.CriteriaSection.MainCriteria.Criteria3")} />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText} primary={t("InterestedPage.CriteriaSection.MainCriteria.Criteria4")} />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText} primary={t("InterestedPage.CriteriaSection.MainCriteria.Criteria5")} />
                </ListItem>
            </List>
            <h4 className={classes.subTitle}>{t("InterestedPage.CriteriaSection.Subtitle2")}</h4>
            <List>
                <ListItem>
                    <ListItemText className={classes.listText}
                        primary={t("InterestedPage.CriteriaSection.AdditionalCriteria.Criteria1")}
                    />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText}
                        primary={t("InterestedPage.CriteriaSection.AdditionalCriteria.Criteria2")}
                    />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText}
                        primary={t("InterestedPage.CriteriaSection.AdditionalCriteria.Criteria3")}
                    />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText}
                        primary={t("InterestedPage.CriteriaSection.AdditionalCriteria.Criteria4")}
                    />
                </ListItem>
                <ListItem>
                    <ListItemText className={classes.listText}
                        primary={t("InterestedPage.CriteriaSection.AdditionalCriteria.Criteria5")}
                    />
                </ListItem>
            </List>
            <h4 className={classes.subTitle}>{t("InterestedPage.CriteriaSection.VetoText")}</h4>
            <h4 className={classes.subTitle}>{t("InterestedPage.CriteriaSection.FinnishRequired")}</h4>
            <h4 className={classes.subTitle}>{t("InterestedPage.CriteriaSection.ResponsesText")}</h4>
        </GridItem>
      </GridContainer>
    </div>
  );
}
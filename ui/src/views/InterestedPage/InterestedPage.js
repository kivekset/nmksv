import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from 'react-i18next';

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";

import InfoSection from "./Sections/InfoSection.js";
import CriteriaSection from "./Sections/CriteriaSection.js";
import ApplicationSection from "./Sections/ApplicationSection.js";

import styles from "assets/jss/material-kit-react/views/interestedPage.js";
import PageWrapper from "components/PageWrapper/PageWrapper.js";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();

  const { t, i18n } = useTranslation();

  return (
    <PageWrapper
      color="transparent"
      changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
    >
      <Parallax
        filter
        image={
          window.innerWidth <= 600
            ? require("assets/img/portugal_600.jpg")
            : require("assets/img/portugal_2400.jpg")
        }
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h2 className={classes.title}>{t("InterestedPage.Title")}</h2>
              <h4 className={classes.ingress}>{t("InterestedPage.Description")}</h4>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
            <InfoSection t={t} />
            <CriteriaSection t={t} />
            <ApplicationSection t={t} i18n={i18n} />
        </div>
      </div>
    </PageWrapper>
  );
}

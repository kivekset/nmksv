import React, { useState, useEffect } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from 'react-i18next';

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";

import styles from "assets/jss/material-kit-react/views/contactPage.js";

// Sections for this page
import PersonSection from "./Sections/PersonSection.js";
import InfoSection from "./Sections/InfoSection.js";
import FeedbackSection from "./Sections/FeedbackSection.js";
import PageWrapper from "components/PageWrapper/PageWrapper.js";

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();

  const { t } = useTranslation();

  // State to tell if all images have been loaded and rendered
  const [images, setImages] = useState(false);

  // URL # position scroll functionality
  useEffect(() => {
    const removeHash = () => {
      const loc = window.location;
      const hist = window.history;
  
      // use modern browser history API
      if (hist && 'pushState' in hist) {
        hist.replaceState('', document.title, loc.pathname + loc.search);
      // fallback for older browsers
      } else {
        // prevent scrolling by storing the page's current scroll offset
        const scrollV = document.body.scrollTop;
        const scrollH = document.body.scrollLeft;
  
        loc.hash = '';
  
        // restore the scroll offset, should be flicker free
        document.body.scrollTop = scrollV;
        document.body.scrollLeft = scrollH;
      }
    };

    // emulate URL anchor page scroll functionality
    (async () => {
      // get URL hash (minus the hash mark)
      const hash = window.location.hash.substring(1);

      // if all images are loaded and there's a hash, scroll to that ID
      if (images && hash && hash.length) {
        // setTimeout and requestAnimationFrame help ensure a true DOM repaint/reflow before we try to scroll
        // - reference: http://stackoverflow.com/a/34999925
        setTimeout(
          window.requestAnimationFrame(function () {
            const el = document.getElementById(hash);

            var headerOffset = 100;
            var elementPosition = el.getBoundingClientRect().top;
            var offsetPosition = elementPosition - headerOffset;
          
            window.scrollTo({
                top: offsetPosition,
                behavior: "smooth"
            });

            // clean up the hash, so we don't scroll on every prop update
            removeHash();
          }),
          0
        );
      }
    })();
  }, [images]);

  return (
    <PageWrapper
      color="transparent"
      changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
    >
      <Parallax
        filter
        image={
          window.innerWidth <= 600
            ? require("assets/img/veneduo_600.jpg")
            : require("assets/img/veneduo_2164.jpg")
        }
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>{t("ContactPage.Title")}</h1>
              <h4>{t("ContactPage.Description")}</h4>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <PersonSection setImagesLoaded={setImages} t={t} />
          <FeedbackSection t={t} />
          <InfoSection t={t} />
        </div>
      </div>
    </PageWrapper>
  );
}

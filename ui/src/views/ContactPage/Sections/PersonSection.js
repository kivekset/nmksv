import React, { useState } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";

import styles from "assets/jss/material-kit-react/views/contactSections/personStyle.js";

import kapteeni from "assets/img/faces/kapteeni.jpg";
import rahastonhoitaja from "assets/img/faces/rahis.jpg";
import bandijaba from "assets/img/faces/bandi.jpg";

const useStyles = makeStyles(styles);

export default function TeamSection({ setImagesLoaded, t }) {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );

  const [state, setState] = useState({
    captainImg: false,
    bandImg: false,
    treasurerImg: false,
  });

  const handleImg = (name) => {
    const tempState = { ...state, [name]: true };
    setState(tempState);

    let allLoaded = true;
    for (const [, value] of Object.entries(tempState)) {
      if (!value) {
        allLoaded = false;
        break;
      }
    }

    if (allLoaded) setImagesLoaded(true);
  };

  return (
    <div className={classes.section} style={{ paddingBottom: "15px" }}>
      <h2 className={classes.title}>{t("ContactPage.PersonSection.Title")}</h2>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img
                  src={kapteeni}
                  alt="il capitano"
                  className={imageClasses}
                  onLoad={() => handleImg("captainImg")}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Tero Hakulinen
                <br />
                <small className={classes.smallTitle}>
                  {t("ContactPage.PersonSection.Captain.Title")}
                </small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  {t("ContactPage.PersonSection.Captain.Description")}
                </p>
                <p className={classes.description}>
                  Telegram:
                  <span>
                    <a
                      href="https://t.me/pitka_koipi_tenho"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      &nbsp;pitka_koipi_tenho
                    </a>
                  </span>
                </p>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img
                  src={rahastonhoitaja}
                  alt="il tesoriere"
                  className={imageClasses}
                  onLoad={() => handleImg("treasurerImg")}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Tuomas Hyypiä
                <br />
                <small className={classes.smallTitle}>
                  {t("ContactPage.PersonSection.Treasurer.Title")}
                </small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  {t("ContactPage.PersonSection.Treasurer.Description")}
                </p>
                <p className={classes.description}>
                  Telegram:
                  <span>
                    <a
                      href="https://t.me/KammenniemenPabloEscobar"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      &nbsp;KammenniemenPabloEscobar
                    </a>
                  </span>
                </p>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img
                  src={bandijaba}
                  alt="il conduttore"
                  className={imageClasses}
                  onLoad={() => handleImg("bandImg")}
                />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Eero Jormalainen
                <br />
                <small className={classes.smallTitle}>
                  {t("ContactPage.PersonSection.BandLeader.Title")}
                </small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  {t("ContactPage.PersonSection.BandLeader.Description")}
                </p>
                <p className={classes.description}>
                  Telegram:
                  <span>
                    <a
                      href="https://t.me/jormalenko"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      &nbsp;jormalenko
                    </a>
                  </span>
                </p>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}

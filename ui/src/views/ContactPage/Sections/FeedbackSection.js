import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import BasicModal from "components/Modal/BasicModal"
import PromptModal from "components/Modal/PromptModal"

import TextField from "@material-ui/core/TextField";

import { callApi } from "../../../services/dataService";

import styles from "assets/jss/material-kit-react/views/contactSections/personStyle.js";

const useStyles = makeStyles(styles);

export default function InfoSection({t}) {
  const classes = useStyles();

  const [state, setState] = useState({
    text: "",
    name: "",
    email: "",
    telegram: "" 
  });

  const [open, setOpen] = useState(false);
  const [openPrompt, setOpenPrompt] = useState(false);
  const [openFieldsMissing, setOpenFieldsMissing] = useState(false);
  const [openError, setOpenError] = useState(false);

  const handleSubmit = event => {
    event.preventDefault();

    if (state.text.trim() === "") {
      setOpenFieldsMissing(true);
      return;
    }

    setOpenPrompt(true);
  };

  const handleSend = () => {

    const requestOptions = {
      method: "POST",
      header: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({...state})
    };
    
    callApi("/feedback", requestOptions)
      .then(() => {
        // Make sure prompt is closed since state might not have been updated yet
        setOpenPrompt(false);
        return setOpen(true);
      })
      .catch(() => {
        // Make sure prompt is closed since state might not have been updated yet
        setOpenPrompt(false);
        return setOpenError(true);
      });
  };

  const feedbackForm = () => {
    return (
      <>
        <form
          className="feedbackForm"
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit}
        >

          <TextField
            required
            id="feedbackText"
            label={t("ContactPage.FeedbackSection.FeedbackLabel")}
            placeholder={t("ContactPage.FeedbackSection.FeedbackPlaceholder")}
            value={state.text}
            onChange={event => {
              setState({ ...state, text: event.target.value });
            }}
            multiline
            variant="outlined"
            inputProps={{ maxLength: 1500 }}
            style={{ margin: 10, width: "80%" }}
            helperText={`${state.text.length}/1500`}
            FormHelperTextProps={{ style: { textAlign: 'right' } }}
          />

          <TextField
            id="feedbackName"
            label={t("ContactPage.FeedbackSection.Name")}
            value={state.name}
            onChange={event => {
              setState({ ...state, name: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="text"
            placeholder={t("ContactPage.FeedbackSection.Name")}
            inputProps={{ maxLength: 40 }}
            style={{ margin: 10, width: "80%" }}
          />

          <TextField
            id="feedbackEmail"
            label={t("ContactPage.FeedbackSection.Email")}
            value={state.email}
            onChange={event => {
              setState({ ...state, email: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="email"
            placeholder={t("ContactPage.FeedbackSection.Email")}
            inputProps={{ maxLength: 100 }}
            style={{ margin: 10, width: "80%" }}
          />

          <TextField
            id="feedbackTelegram"
            label={t("ContactPage.FeedbackSection.Telegram")}
            value={state.telegram}
            onChange={event => {
              setState({ ...state, telegram: event.target.value });
            }}
            onKeyPress={e => { e.key === 'Enter' && e.preventDefault(); }}
            variant="outlined"
            type="text"
            placeholder={t("ContactPage.FeedbackSection.Telegram")}
            inputProps={{ maxLength: 50 }}
            style={{ margin: 10, width: "80%" }}
          />

          <Button
            type="submit"
            color="primary"
            variant="contained"
            id="btnFeedback"
            style={{ margin: 10 }}
          >
            {t("ContactPage.FeedbackSection.SendButton")}
          </Button>

        </form>

        <BasicModal 
            open={openFieldsMissing}
            setOpen={setOpenFieldsMissing}
            title={t("ContactPage.FeedbackSection.FieldsMissing.Title")}
            text={[t("ContactPage.FeedbackSection.FieldsMissing.Description")]}
          />

          <BasicModal 
            open={openError}
            setOpen={setOpenError}
            title={t("ContactPage.FeedbackSection.SendError.Title")}
            text={[t("ContactPage.FeedbackSection.SendError.Description")]}
          />

          <PromptModal
            open={openPrompt}
            onAccept={() => handleSend()}
            setOpen={setOpenPrompt}
            title={t("ContactPage.FeedbackSection.Confirmation.Title")}
            text={[t("ContactPage.FeedbackSection.Confirmation.Description")]}
          />

          <BasicModal 
            open={open}
            setOpen={setOpen}
            title={t("ContactPage.FeedbackSection.FeedbackSaved.Title")}
            text={[t("ContactPage.FeedbackSection.FeedbackSaved.Description")]}
            onClose={() => {
              setOpen(false);
              setState({ text: "", name: "", email: "", telegram: "" })
            }}
          />
        </>
    );
  };

  return (
    <div className={classes.section} style={{paddingTop: "0px"}}>
      <h2 className={classes.title} id="palaute">{t("ContactPage.FeedbackSection.Title")}</h2>
      <GridContainer justifyContent="center">
        <GridItem xs={12} sm={12} md={8}>
          <h5 className={classes.description}>{t("ContactPage.FeedbackSection.Description")}</h5>
          {feedbackForm()}
        </GridItem>
      </GridContainer>
    </div>
  );
}

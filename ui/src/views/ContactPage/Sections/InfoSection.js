import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/views/contactSections/personStyle.js";

const useStyles = makeStyles(styles);

export default function InfoSection({t}) {
  const classes = useStyles();
  return (
    <div className={classes.section} style={{paddingTop: "0px"}}>
      <h2 className={classes.title}>{t("ContactPage.InfoSection.Title")}</h2>
      <GridContainer justifyContent="center">
        <GridItem xs={12} sm={12} md={8}>
          <h5 className={classes.description}>{t("ContactPage.InfoSection.Description1")}</h5>
          <h4 className={classes.title}>kapteenisto(at)nmksv.org</h4>
          <h5 className={classes.description}>{t("ContactPage.InfoSection.Description2")}</h5>
        </GridItem>
      </GridContainer>
      <GridContainer justifyContent="center">
        <GridItem xs={12} sm={12} md={8}>
          <Button
            color="primary"
            href="https://www.facebook.com/NMKSV/"
            target="_blank"
            className={classes.margin10}
          >
            <i className={classes.socialIcons + " fab fa-facebook"} /> Facebook
          </Button>
          <Button
            color="primary"
            href="https://www.instagram.com/urrrheilujoukkue/"
            target="_blank"
            className={classes.margin20}
          >
            <i className={classes.socialIcons + " fab fa-instagram"} />{" "}
            Instagram
          </Button>
        </GridItem>
      </GridContainer>
    </div>
  );
}

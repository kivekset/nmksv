import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import styles from "assets/jss/material-kit-react/views/history.js";

const useStyles = makeStyles(styles);

export default function HistorySection({t}) {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <div>
        <GridContainer justifyContent="center">
          <GridItem xs={12} sm={12} md={8}>
            <h2 className={classes.sectionTitle}>{t("HistoryPage.HistorySection.Title")}</h2>
            <h3 className={classes.year}>2006-2007</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2006.Part1")}</p>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2006.Part2")}</p>

            <h3 className={classes.year}>2007-2008</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2007.Part1")}</p>

            <h3 className={classes.year}>2008-2009</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2008.Part1")}</p>

            <h3 className={classes.year}>2009-2010</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2009.Part1")}</p>

            <h3 className={classes.year}>2010-2011</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2010.Part1")}</p>

            <h3 className={classes.year}>2011-2012</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2011.Part1")}</p>

            <h3 className={classes.year}>2012-2013</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2012.Part1")}</p>

            <h3 className={classes.year}>2013-2014</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2013.Part1")}</p>

            <h3 className={classes.year}>2014-2015</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2014.Part1")}</p>

            <h3 className={classes.year}>2015-2016</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2015.Part1")}</p>

            <h3 className={classes.year}>2016-2017</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2016.Part1")}</p>

            <h3 className={classes.year}>2017-2018</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2017.Part1")}</p>

            <h3 className={classes.year}>2018-2019</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2018.Part1")}</p>

            <h3 className={classes.year}>2019-2020</h3>
            <p className={classes.paragraph}>{t("HistoryPage.HistorySection.2019.Part1")}</p>

            <h3 className={classes.year}>2020-2021</h3>
            <p className={classes.paragraph}>
              {t("HistoryPage.HistorySection.2020.Part1")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2020.Part2")}
            </p>

            <h3 className={classes.year}>2021-2022</h3>
            <p className={classes.paragraph}>
              {t("HistoryPage.HistorySection.2021.Part1")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2021.Part2")}
            </p>

            <h3 className={classes.year}>2022-2023</h3>
            <p className={classes.paragraph}>
              {t("HistoryPage.HistorySection.2022.Part1")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2022.Part2")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2022.Part3")}
            </p>

            <h3 className={classes.year}>2023-2024</h3>
            <p className={classes.paragraph}>
              {t("HistoryPage.HistorySection.2023.Part1")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2023.Part2")}
              <br></br> <br></br>
              {t("HistoryPage.HistorySection.2023.Part3")}
            </p>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}

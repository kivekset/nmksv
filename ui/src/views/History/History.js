import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import { useTranslation } from 'react-i18next';

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";

import styles from "assets/jss/material-kit-react/views/history.js";

// Sections for this page
import HistorySection from "./Sections/HistorySection.js";
import PageWrapper from "components/PageWrapper/PageWrapper.js";

const useStyles = makeStyles(styles);

export default function History(props)
{  const classes = useStyles();

  const { t } = useTranslation();

  return (
    <PageWrapper
      color="transparent"
      changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
    >
      <Parallax
        filter
        image={
          window.innerWidth <= 600
            ? require("assets/img/henniseta_600.jpg")
            : require("assets/img/henniseta_2164.jpg")
        }
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>{t("HistoryPage.Title")}</h1>
              <h4 className={classes.ingress}>
                {t("HistoryPage.Description1")}<a style={{ color: "blue" }} href="/ennen">{t("HistoryPage.Ennen")}</a>{t("HistoryPage.Description2")}
              </h4>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <HistorySection t={t} />
        </div>
      </div>
    </PageWrapper>
  );
}
